package com.twoswap.com.pojo_class.profile_myexchanges_pojo;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class ProfileMyExchangesData {

    private String swapThumbnailImageUrl;

    private String negotiable;

    private String swapPostId;

    private String acceptedOn;

    private String swapProductName;

    private String currency;

    private String city;

    private String imageUrl4;

    private String description;

    private String longitude;

    private String place;

    private String thumbnailImageUrl;

    private String condition;

    private String mainUrl;

    private String imageUrl3;

    private String imageUrl2;

    private String postId;

    private String imageUrl1;

    private String swapMainUrl;

    private String countrySname;

    private String swapImageUrl2;

    private String price;

    private String swapImageUrl1;

    private String swapImageUrl4;

    private String swapImageUrl3;

    private String priceInUSD;

    private String latitude;

    private String productsTagged;

    private String productName;

    public String getSwapThumbnailImageUrl ()
    {
        return swapThumbnailImageUrl;
    }

    public void setSwapThumbnailImageUrl (String swapThumbnailImageUrl)
    {
        this.swapThumbnailImageUrl = swapThumbnailImageUrl;
    }

    public String getNegotiable ()
    {
        return negotiable;
    }

    public void setNegotiable (String negotiable)
    {
        this.negotiable = negotiable;
    }

    public String getSwapPostId ()
    {
        return swapPostId;
    }

    public void setSwapPostId (String swapPostId)
    {
        this.swapPostId = swapPostId;
    }

    public String getAcceptedOn ()
    {
        return acceptedOn;
    }

    public void setAcceptedOn (String acceptedOn)
    {
        this.acceptedOn = acceptedOn;
    }

    public String getSwapProductName ()
    {
        return swapProductName;
    }

    public void setSwapProductName (String swapProductName)
    {
        this.swapProductName = swapProductName;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getImageUrl4 ()
    {
        return imageUrl4;
    }

    public void setImageUrl4 (String imageUrl4)
    {
        this.imageUrl4 = imageUrl4;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getPlace ()
    {
        return place;
    }

    public void setPlace (String place)
    {
        this.place = place;
    }

    public String getThumbnailImageUrl ()
    {
        return thumbnailImageUrl;
    }

    public void setThumbnailImageUrl (String thumbnailImageUrl)
    {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }

    public String getCondition ()
    {
        return condition;
    }

    public void setCondition (String condition)
    {
        this.condition = condition;
    }

    public String getMainUrl ()
    {
        return mainUrl;
    }

    public void setMainUrl (String mainUrl)
    {
        this.mainUrl = mainUrl;
    }

    public String getImageUrl3 ()
    {
        return imageUrl3;
    }

    public void setImageUrl3 (String imageUrl3)
    {
        this.imageUrl3 = imageUrl3;
    }

    public String getImageUrl2 ()
    {
        return imageUrl2;
    }

    public void setImageUrl2 (String imageUrl2)
    {
        this.imageUrl2 = imageUrl2;
    }

    public String getPostId ()
    {
        return postId;
    }

    public void setPostId (String postId)
    {
        this.postId = postId;
    }

    public String getImageUrl1 ()
    {
        return imageUrl1;
    }

    public void setImageUrl1 (String imageUrl1)
    {
        this.imageUrl1 = imageUrl1;
    }

    public String getSwapMainUrl ()
    {
        return swapMainUrl;
    }

    public void setSwapMainUrl (String swapMainUrl)
    {
        this.swapMainUrl = swapMainUrl;
    }

    public String getCountrySname ()
    {
        return countrySname;
    }

    public void setCountrySname (String countrySname)
    {
        this.countrySname = countrySname;
    }

    public String getSwapImageUrl2 ()
    {
        return swapImageUrl2;
    }

    public void setSwapImageUrl2 (String swapImageUrl2)
    {
        this.swapImageUrl2 = swapImageUrl2;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getSwapImageUrl1 ()
    {
        return swapImageUrl1;
    }

    public void setSwapImageUrl1 (String swapImageUrl1)
    {
        this.swapImageUrl1 = swapImageUrl1;
    }

    public String getSwapImageUrl4 ()
    {
        return swapImageUrl4;
    }

    public void setSwapImageUrl4 (String swapImageUrl4)
    {
        this.swapImageUrl4 = swapImageUrl4;
    }

    public String getSwapImageUrl3 ()
    {
        return swapImageUrl3;
    }

    public void setSwapImageUrl3 (String swapImageUrl3)
    {
        this.swapImageUrl3 = swapImageUrl3;
    }

    public String getPriceInUSD ()
    {
        return priceInUSD;
    }

    public void setPriceInUSD (String priceInUSD)
    {
        this.priceInUSD = priceInUSD;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getProductsTagged ()
    {
        return productsTagged;
    }

    public void setProductsTagged (String productsTagged)
    {
        this.productsTagged = productsTagged;
    }

    public String getProductName ()
    {
        return productName;
    }

    public void setProductName (String productName)
    {
        this.productName = productName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [swapThumbnailImageUrl = "+swapThumbnailImageUrl+", negotiable = "+negotiable+", swapPostId = "+swapPostId+", acceptedOn = "+acceptedOn+", swapProductName = "+swapProductName+", currency = "+currency+", city = "+city+", imageUrl4 = "+imageUrl4+", description = "+description+", longitude = "+longitude+", place = "+place+", thumbnailImageUrl = "+thumbnailImageUrl+", condition = "+condition+", mainUrl = "+mainUrl+", imageUrl3 = "+imageUrl3+", imageUrl2 = "+imageUrl2+", postId = "+postId+", imageUrl1 = "+imageUrl1+", swapMainUrl = "+swapMainUrl+", countrySname = "+countrySname+", swapImageUrl2 = "+swapImageUrl2+", price = "+price+", swapImageUrl1 = "+swapImageUrl1+", swapImageUrl4 = "+swapImageUrl4+", swapImageUrl3 = "+swapImageUrl3+", priceInUSD = "+priceInUSD+", latitude = "+latitude+", productsTagged = "+productsTagged+", productName = "+productName+"]";
    }

}
