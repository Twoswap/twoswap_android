package com.twoswap.com.main.camera_kit;


import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraView;
import com.twoswap.com.R;
import com.twoswap.com.adapter.ImagesHorizontalRvAdap;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.main.activity.GalleryActivity;
import com.twoswap.com.main.activity.PostProductActivity;
import com.twoswap.com.utility.CapturedImage;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.RunTimePermission;
import com.twoswap.com.utility.VariableConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class CameraKitActivity extends AppCompatActivity implements ClickListener {

    private static final String TAG = CameraKitActivity.class.getSimpleName();

    @BindView(R.id.camera)
    CameraView camera;

    private int cameraMethod = CameraKit.Constants.METHOD_STANDARD;
    private boolean cropOutput = false;

    private Activity mActivity;
    public ImagesHorizontalRvAdap imagesHorizontalRvAdap;
    public ArrayList<CapturedImage> arrayListImgPath;
    public boolean isToCaptureImage, isFromUpdatePost;
    private NotificationMessageDialog mNotificationMessageDialog;
    private String[] permissionsArray;
    private RunTimePermission runTimePermission;

    /**
     * Xml Variables
     */
    RelativeLayout rL_rootview;
    public TextView tV_upload;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_kit);
        ButterKnife.bind(this);
        mActivity = CameraKitActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        camera.setMethod(cameraMethod);
        camera.setCropOutput(cropOutput);
        initVarible();

    }

    private void initVarible(){

        isToCaptureImage=false;

        permissionsArray = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE,RECORD_AUDIO};
        runTimePermission = new RunTimePermission(mActivity, permissionsArray, true);
        if (!runTimePermission.checkPermissions(permissionsArray)){
            runTimePermission.requestPermission();
        }

        // receive datas from last activity
        Intent intent=getIntent();
        isFromUpdatePost=intent.getBooleanExtra("isUpdatePost",false);

        // set status bar color
        CommonClass.statusBarColor(mActivity);

        arrayListImgPath = new ArrayList<>();

        // root element
        rL_rootview = (RelativeLayout) findViewById(R.id.rL_rootview);


        // Adapter
        tV_upload= (TextView) findViewById(R.id.tV_upload);
        imagesHorizontalRvAdap=new ImagesHorizontalRvAdap(mActivity,arrayListImgPath,tV_upload,isToCaptureImage);
        RecyclerView rV_cameraImages = (RecyclerView) findViewById(R.id.rV_cameraImages);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false);
        rV_cameraImages.setLayoutManager(linearLayoutManager);
        rV_cameraImages.setAdapter(imagesHorizontalRvAdap);

    }

    @OnClick(R.id.rL_done)
    public void done(){
        Intent intent;
        ArrayList<String> galleryImagePaths=new ArrayList<>();
        ArrayList<Integer> rotationAngles=new ArrayList<>();

        for(int i=0;i<arrayListImgPath.size();i++)
        {
            galleryImagePaths.add(arrayListImgPath.get(i).getImagePath());
            rotationAngles.add(arrayListImgPath.get(i).getRotateAngle());
        }

        if (isFromUpdatePost)
        {
            intent =new Intent();
            intent.putExtra("arrayListImgPath",galleryImagePaths);
            intent.putExtra("rotationAngles",rotationAngles);
            setResult(VariableConstants.UPDATE_IMAGE_REQ_CODE,intent);
            onBackPressed();
        }
        else
        {
            if (arrayListImgPath.size()>0) {
                intent=new Intent(mActivity,PostProductActivity.class);
                intent.putExtra("arrayListImgPath",galleryImagePaths);
                intent.putExtra("rotationAngles",rotationAngles);
                startActivityForResult(intent,VariableConstants.CAMERA_IMAGE_LIST_REQ);
            }
        }
    }

    @OnClick(R.id.rL_cancel_btn)
    public void cancle(){
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.start();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        camera.stop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onItemClick(View view, int position) {
        Intent intent;
        switch (position){
            case 1:
                System.out.println(TAG+" "+"captured image size="+arrayListImgPath.size());
                intent = new Intent(mActivity, GalleryActivity.class);
                ArrayList<String> galleryImagePaths = new ArrayList<>();
                ArrayList<Integer> rotationAngles=new ArrayList<>();

                for (int i = 0; i < arrayListImgPath.size(); i++) {
                    galleryImagePaths.add(arrayListImgPath.get(i).getImagePath());
                    rotationAngles.add(arrayListImgPath.get(i).getRotateAngle());
                }

                intent.putExtra("arrayListImgPath", galleryImagePaths);
                intent.putExtra("rotationAngles",rotationAngles);
                startActivityForResult(intent, VariableConstants.SELECT_GALLERY_IMG_REQ_CODE);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(TAG+" "+"on activity result called..."+requestCode);
        if (data != null)
        {
            switch (requestCode)
            {
                case VariableConstants.SELECT_GALLERY_IMG_REQ_CODE :
                    if (data.getStringArrayListExtra("arrayListImgPath")!=null && data.getStringArrayListExtra("arrayListImgPath").size()>0)
                    {
                        tV_upload.setTextColor(ContextCompat.getColor(mActivity, R.color.pink_color));
                        arrayListImgPath.clear();

                        ArrayList<String > imagePathsFromGallery=data.getStringArrayListExtra("arrayListImgPath");
                        CapturedImage image;


                        for(int i=0;i<imagePathsFromGallery.size();i++) {
                            image = new CapturedImage();

                            image.setRotateAngle(0);
                            image.setImagePath(imagePathsFromGallery.get(i));
                            arrayListImgPath.add(image);
                        }
//                        arrayListImgPath.addAll(data.getStringArrayListExtra("arrayListImgPath"));
                        imagesHorizontalRvAdap.notifyDataSetChanged();
                    }
                    else  tV_upload.setTextColor(ContextCompat.getColor(mActivity, R.color.reset_button_bg));
                    break;
                case VariableConstants.CAMERA_IMAGE_LIST_REQ:
                    ArrayList<CapturedImage> list= (ArrayList<CapturedImage>) data.getSerializableExtra("arrayListImgPath");
                    if(list!=null){
                        arrayListImgPath.clear();
                        arrayListImgPath.addAll(list);
                        imagesHorizontalRvAdap.notifyDataSetChanged();
                    }

                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case VariableConstants.PERMISSION_REQUEST_CODE :
                System.out.println("grant result="+grantResults.length);
                if (grantResults.length>0)
                {
                    for (int count=0;count<grantResults.length;count++)
                    {
                        if (grantResults[count]!= PackageManager.PERMISSION_GRANTED)
                            runTimePermission.allowPermissionAlert(permissions[count]);

                    }
                    System.out.println("isAllPermissionGranted="+runTimePermission.checkPermissions(permissionsArray));
                    if (runTimePermission.checkPermissions(permissionsArray)) {
                        System.out.println(TAG+" "+"openCamera from onRequestPermissionsResult");
                    }
                }
        }
    }

    private static abstract class CameraSetting {

        abstract String getTitle();
        abstract String getValue();
        abstract void toggle();

    }

    private CameraSetting captureMethodSetting = new CameraSetting() {
        @Override
        String getTitle() {
            return "ckMethod";
        }

        @Override
        String getValue() {
            switch (cameraMethod) {
                case CameraKit.Constants.METHOD_STANDARD: {
                    return "standard";
                }

                case CameraKit.Constants.METHOD_STILL: {
                    return "still";
                }

                default: return null;
            }
        }

        @Override
        void toggle() {
            if (cameraMethod == CameraKit.Constants.METHOD_STANDARD) {
                cameraMethod = CameraKit.Constants.METHOD_STILL;
            } else {
                cameraMethod = CameraKit.Constants.METHOD_STANDARD;
            }
            camera.setMethod(cameraMethod);
        }
    };

    private CameraSetting cropSetting = new CameraSetting() {
        @Override
        String getTitle() {
            return "ckCropOutput";
        }

        @Override
        String getValue() {
            if (cropOutput) {
                return "true";
            } else {
                return "false";
            }
        }

        @Override
        void toggle() {
            cropOutput = !cropOutput;
            camera.setCropOutput(cropOutput);
        }
    };
}
