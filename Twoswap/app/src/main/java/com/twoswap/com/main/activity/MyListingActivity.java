package com.twoswap.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twoswap.com.R;
import com.twoswap.com.adapter.MyListingAdap;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.main.activity.products.ProductDetailsActivity;
import com.twoswap.com.main.camera_kit.CameraKitActivity;
import com.twoswap.com.mqttchat.Activities.ChatMessageScreen;
import com.twoswap.com.mqttchat.AppController;
import com.twoswap.com.mqttchat.Utilities.MqttEvents;
import com.twoswap.com.mqttchat.Utilities.Utilities;
import com.twoswap.com.pojo_class.profile_selling_pojo.ProfileSellingData;
import com.twoswap.com.pojo_class.profile_selling_pojo.ProfileSellingMainPojo;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.ProductItemClickListener;
import com.twoswap.com.utility.SessionManager;
import com.twoswap.com.utility.SpacesItemDecoration;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/*
* In this Activity use same data of profile selling data.
* */

public class MyListingActivity extends AppCompatActivity implements View.OnClickListener,ProductItemClickListener{

    private Activity mActivity;
    private static final String TAG=MyListingActivity.class.getSimpleName();
    private NotificationMessageDialog mNotificationMessageDialog;
    private RecyclerView rV_myListing;
    private RelativeLayout rL_noProductFound;
    private ArrayList<ProfileSellingData> arrayListSellingDatas;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int pageIndex;
    private ProgressBar progress_bar_profile;
    private StaggeredGridLayoutManager gridLayoutManager;
    private MyListingAdap myListingAdap;
    private SessionManager mSessionManager;
    private String memberName;
    // Load more var
    private boolean isLoadingRequired;
    private int visibleItemCount,totalItemCount,visibleThreshold=5;

    private String postId="";
    private String member_name="";
    private String receiverMqttId ="";
    private String productName;
    private String productPicUrl;
    private String currency;
    private String price;
    private String memberPicUrl;
    private String fromChatScreen="0";
    private String latitude="",longitude="";
    private RelativeLayout root_MyListing;
    private RelativeLayout rL_swap;
    private ProgressBar progress_bar_save;
    private int mSelected=-1;
    private String swapPostId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_listing);
        mActivity=MyListingActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        mSessionManager=new SessionManager(mActivity);
        memberName=mSessionManager.getUserName();
        initVar();
    }

    public void initVar(){
        
        Intent intent=getIntent();
        Bundle  data=intent.getExtras();
        String place;
        
        if(data!=null) {
            productPicUrl = data.getString("productPicUrl");
            productName = data.getString("productName");
            place = data.getString("place");
            postId = data.getString("postId");
            latitude = data.getString("latitude");
            longitude = data.getString("longitude");
            String currency = data.getString("currency");
            price = data.getString("price");
            memberPicUrl = data.getString("memberPicUrl");
            member_name = data.getString("membername");
            receiverMqttId = data.getString("receiverMqttId");
            String negotiable = data.getString("negotiable");
        }

        //root view
        root_MyListing = (RelativeLayout) findViewById(R.id.root_MyListing);
        
        // back button
        RelativeLayout rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);

        progress_bar_save=(ProgressBar)findViewById(R.id.progress_bar_save);

        rV_myListing = (RecyclerView)findViewById(R.id.rV_myListing);

        pageIndex=0;
        // set space equility between recycler view items
        int spanCount = 2; // 2 columns
        int spacing = 10; // 50px
        rV_myListing.addItemDecoration(new SpacesItemDecoration(spanCount, spacing));

        mSwipeRefreshLayout= (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        progress_bar_profile= (ProgressBar)findViewById(R.id.progress_bar);

        arrayListSellingDatas=new ArrayList<>();
        myListingAdap=new MyListingAdap(mActivity,arrayListSellingDatas,this);
        gridLayoutManager=new StaggeredGridLayoutManager(2,1);
        rV_myListing.setLayoutManager(gridLayoutManager);
        rV_myListing.setAdapter(myListingAdap);

        // set empty favourite icon
        rL_noProductFound= (RelativeLayout) findViewById(R.id.rL_noProductFound);
        rL_noProductFound.setVisibility(View.GONE);

        // set empty favourite icon
        rL_noProductFound= (RelativeLayout) findViewById(R.id.rL_noProductFound);
        rL_noProductFound.setVisibility(View.GONE);

        ImageView iV_default_icon= (ImageView) findViewById(R.id.iV_default_icon);
        iV_default_icon.setImageResource(R.drawable.empty_selling_icon);

        TextView tV_no_ads= (TextView) findViewById(R.id.tV_no_ads);
        tV_no_ads.setText(getResources().getString(R.string.no_ads_yet));

        TextView tV_snapNpost= (TextView) findViewById(R.id.tV_snapNpost);
        tV_snapNpost.setText(getResources().getString(R.string.snapNpostIn));

        TextView tV_start_discovering= (TextView) findViewById(R.id.tV_start_discovering);
        tV_start_discovering.setText(getResources().getString(R.string.start_selling));

        rL_swap = (RelativeLayout) findViewById(R.id.rL_swap);
        rL_swap.setOnClickListener(this);

        RelativeLayout rL_start_selling= (RelativeLayout) findViewById(R.id.rL_start_selling);

        rL_start_selling.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(mActivity, CameraActivity.class));
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    startActivity(new Intent(mActivity, Camera2Activity.class));
                else*/
                    startActivity(new Intent(mActivity, CameraKitActivity.class));
            }
        });

        // call api call method
        if (CommonClass.isNetworkAvailable(mActivity))
        {
            progress_bar_profile.setVisibility(View.VISIBLE);
            if(mSessionManager.getIsUserLoggedIn()) {
                profilePosts(pageIndex);
            }
        }
        else  CommonClass.showSnackbarMessage(root_MyListing,getResources().getString(R.string.NoInternetAccess));

        // pull to refresh
        mSwipeRefreshLayout.setColorSchemeResources(R.color.pink_color);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageIndex=0;
                arrayListSellingDatas.clear();
                myListingAdap.notifyDataSetChanged();
                if(mSessionManager.getIsUserLoggedIn()){
                    profilePosts(pageIndex);
                }
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            //  back button
            case R.id.rL_back_btn:
                onBackPressed();
                break;

            //  swap button
            case R.id.rL_swap:
                if(mSelected>=0)
                    swapOffer();
                break;
        }
    }

    @Override
    public void onItemClick(int pos, ImageView imageView) {
        mSelected=pos;
        swapPostId=arrayListSellingDatas.get(mSelected).getPostId();
        // Toast.makeText(mActivity,pos+"",Toast.LENGTH_SHORT).show();
    }

    private void profilePosts(int offset)
    {
        if (CommonClass.isNetworkAvailable(mActivity))
        {
            int limit=20;
            offset=limit*offset;
            JSONObject request_datas = new JSONObject();
            try {
                request_datas.put("token", mSessionManager.getAuthToken());
                request_datas.put("limit",limit);
                request_datas.put("offset",offset);
                request_datas.put("sold", "0");
                request_datas.put("membername",memberName);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url;

            Log.d("username111",memberName);
            Log.d("username112",mSessionManager.getUserName());

            if(mSessionManager.getUserName().equals(memberName)) {
                url= ApiUrl.PROFILE_POST;
                Log.d("username113",url);
            }
            else {
                url=ApiUrl.PROFILE_POST+memberName;
                Log.d("username114",url);
            }


            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.POST, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    progress_bar_profile.setVisibility(View.GONE);
                    System.out.println(TAG + " " + "profile selling res=" + result);
                    mSwipeRefreshLayout.setRefreshing(false);

                    /*ExplorePojoMain explorePojoMain;
                    Gson gson = new Gson();
                    explorePojoMain = gson.fromJson(result, ExplorePojoMain.class);*/

                    ProfileSellingMainPojo profileSellingMainPojo;
                    Gson gson = new Gson();
                    profileSellingMainPojo = gson.fromJson(result,ProfileSellingMainPojo.class);

                    switch (profileSellingMainPojo.getCode()) {
                        // success
                        case "200":
                            if (profileSellingMainPojo.getData() != null && profileSellingMainPojo.getData().size() > 0)
                            {

                                rL_noProductFound.setVisibility(View.GONE);
                                arrayListSellingDatas.addAll(profileSellingMainPojo.getData());
                                isLoadingRequired=arrayListSellingDatas.size()>14;
                                myListingAdap.notifyDataSetChanged();

                                // Load more
                                rV_myListing.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                    @Override
                                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                        super.onScrolled(recyclerView, dx, dy);

                                        int[] firstVisibleItemPositions = new int[2];
                                        totalItemCount = gridLayoutManager.getItemCount();
                                        visibleItemCount = gridLayoutManager.findLastVisibleItemPositions(firstVisibleItemPositions)[0];

                                        if (isLoadingRequired && totalItemCount<=(visibleItemCount+visibleThreshold))
                                        {
                                            isLoadingRequired=false;
                                            pageIndex=pageIndex+1;
                                            mSwipeRefreshLayout.setRefreshing(true);
                                            profilePosts(pageIndex);
                                        }
                                    }
                                });
                            }
                            break;

                        // no more data found
                        case "204":
                            mSwipeRefreshLayout.setRefreshing(false);
                            if(arrayListSellingDatas.size()==0){
                                rL_noProductFound.setVisibility(View.VISIBLE);
                            }
                            break;

                        // auth token expired
                        case "401" :
                            mSwipeRefreshLayout.setRefreshing(false);
                            CommonClass.sessionExpired(mActivity);
                            break;

                        // Any error
                        default:
                            mSwipeRefreshLayout.setRefreshing(false);
                            CommonClass.showTopSnackBar(root_MyListing,profileSellingMainPojo.getMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    progress_bar_profile.setVisibility(View.GONE);
                    CommonClass.showSnackbarMessage(root_MyListing,error);
                }
            });
        }
        else CommonClass.showSnackbarMessage(root_MyListing,getResources().getString(R.string.NoInternetAccess));
    }

    /*
   * Handel make offer button data.*/
    private void handelButton(boolean isEnable)
    {
        if(isEnable)
        {
            progress_bar_save.setVisibility(View.VISIBLE);
            rL_swap.setEnabled(false);
        }else
        {
            rL_swap.setEnabled(true);
            progress_bar_save.setVisibility(View.GONE);
        }
    }

    /*
     *swap offer api */
    private JSONObject requestDats;

    private void swapOffer()
    {
        if(receiverMqttId==null||receiverMqttId.isEmpty())
        {
            Toast.makeText(this, R.string.mqtt_user_not_text,Toast.LENGTH_SHORT).show();
            return;
        }

        if (CommonClass.isNetworkAvailable(mActivity))
        {
            handelButton(true);
            requestDats = new JSONObject();
            try {
                requestDats.put("token",mSessionManager.getAuthToken());
                //requestDats.put("offerStatus","1");
                requestDats.put("postId",postId);
                requestDats.put("swapPostId",swapPostId);
                requestDats.put("swapStatus","1");
                requestDats.put("price",price);
                //requestDats.put("type","0");
                requestDats.put("membername",member_name);
                requestDats.put("sendchat",createMessageObject(price));
            } catch (Exception e) {
                e.printStackTrace();
                handelButton(false);
            }

            OkHttp3Connection.doOkHttp3Connection("",ApiUrl.SWAP_OFFER,OkHttp3Connection.Request_type.POST, requestDats, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag)
                {
                    System.out.println(TAG+" swap offer respone="+result);
                    handelButton(false);
                    try {
                        handelResponse(result);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(String error, String user_tag)
                {
                    handelButton(false);
                    CommonClass.showSnackbarMessage(root_MyListing,error);
                }
            });

        }else
            CommonClass.showSnackbarMessage(root_MyListing,getResources().getString(R.string.NoInternetAccess));
    }

    /*
     *creating the msg object */
    private String doucumentId;
    private boolean isNew=false;
    private JSONObject createMessageObject(String amount) throws Exception
    {
        doucumentId = AppController.getInstance().findDocumentIdOfReceiver(receiverMqttId,postId);
        if (doucumentId.isEmpty())
        {
            isNew=true;
            doucumentId =AppController.findDocumentIdOfReceiver(receiverMqttId, Utilities.tsInGmt(),member_name,memberPicUrl,postId,false,"","", productPicUrl,productName,price,false,false);
        }else
        {
            isNew=false;
            AppController.getInstance().getDbController().updateChatDetails(doucumentId,member_name,memberPicUrl);
        }
        Log.d("log88",""+amount);
        String swapText = getString(R.string.i_would_like_to_offer_my)+" "+arrayListSellingDatas.get(mSelected).getProductName()+" "+getString(R.string.in_place_of_your)+" "+productName;
        byte[] byteArray = swapText.getBytes("UTF-8");
        String messageInbase64 = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
        String tsForServer = Utilities.tsInGmt();
        String  tsForServerEpoch = new Utilities().gmtToEpoch(tsForServer);
        JSONObject messageData=new JSONObject();
        messageData.put("name", mSessionManager.getUserName());
        messageData.put("from",mSessionManager.getmqttId());
        messageData.put("to",receiverMqttId);
        messageData.put("payload",messageInbase64 );
        messageData.put("type","17"); // for swap we use 17
        messageData.put("offerType","5");// here we send random which is not use
        messageData.put( "id",tsForServerEpoch);
        messageData.put("secretId",postId);
        messageData.put("thumbnail","");
        messageData.put("userImage",mSessionManager.getUserImage());
        messageData.put("toDocId",doucumentId);
        messageData.put("dataSize",1);
        messageData.put("isSold","0");
        messageData.put("productUrl", productPicUrl);
        messageData.put("productId",postId);
        messageData.put("productName",productName);
        messageData.put("productPrice",""+price);
        messageData.put("swapType","1");
        messageData.put("isSwap","1");
        messageData.put("swapProductName",arrayListSellingDatas.get(mSelected).getProductName());
        messageData.put("swapProductUrl",arrayListSellingDatas.get(mSelected).getThumbnailImageUrl());
        messageData.put("swapProductId",arrayListSellingDatas.get(mSelected).getPostId());
        messageData.put("swapTextMessage",swapText);
        messageData.put("isRejected","0");
        return messageData;
    }

    /*
    * Handling the response data.*/
    private void handelResponse(String resaponse) throws Exception
    {
        JSONObject jsonObject = new JSONObject(resaponse);
        String code_Data = jsonObject.getString("code");
        switch (code_Data) {
            // success
            case "200":
                AppController.getInstance().sendMessageToFcm(MqttEvents. OfferMessage+"/"+requestDats.getJSONObject("sendchat").getString("to"),requestDats.getJSONObject("sendchat"));
                Intent intent;
                intent = new Intent(this,ChatMessageScreen.class);
                intent.putExtra("isNew",isNew);
                intent.putExtra("receiverUid", receiverMqttId);
                intent.putExtra("receiverName",member_name);
                intent.putExtra("documentId",doucumentId);
                intent.putExtra("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                intent.putExtra("receiverImage",memberPicUrl);
                intent.putExtra("colorCode", AppController.getInstance().getColorCode(1% 19));
//                intent.putExtra("swapProduct",true);
                intent.putExtra("productPicUrl",productPicUrl);
                intent.putExtra("swapProductpicUrl",arrayListSellingDatas.get(mSelected).getMainUrl());
                intent.putExtra("swapProductId",arrayListSellingDatas.get(mSelected).getPostId());
                String swapText = getString(R.string.i_would_like_to_offer_my)+" "+arrayListSellingDatas.get(mSelected).getProductName()+" "+getString(R.string.in_place_of_your)+" "+productName;
                intent.putExtra("swapTextMsg",swapText);

                if(fromChatScreen.equals("0"))
                {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else
                {
                    ProductDetailsActivity.productDetailsActivity.finish();
                }
                mActivity.finish();
                break;
            // auth token expired
            case "401":
                CommonClass.sessionExpired(mActivity);
                break;
            case "409":
                CommonClass.showTopSnackBar(root_MyListing,getString(R.string.already_sold_Text));
                break;
            // error
            default:
                CommonClass.showTopSnackBar(root_MyListing,jsonObject.getString("message"));
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

}
