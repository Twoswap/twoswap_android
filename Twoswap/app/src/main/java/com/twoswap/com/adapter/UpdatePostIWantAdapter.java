package com.twoswap.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.main.activity.UpdatePostActivity;
import com.twoswap.com.pojo_class.product_details_pojo.SwapPost;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class UpdatePostIWantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<SwapPost> swapPostArrayList;
    private UpdatePostActivity mActivity;
    private static final int TYPE_DATA = 0;
    private static final int TYPE_ADD_MORE = 1;

    public UpdatePostIWantAdapter(ArrayList<SwapPost> swapPostArrayList, UpdatePostActivity mActivity) {
        this.swapPostArrayList = swapPostArrayList;
        this.mActivity = mActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case TYPE_DATA:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_iwant_item, parent, false);
                holder = new MyViewHolder(itemView);
                break;

            case TYPE_ADD_MORE:
                View empty_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_iwant_add_more, parent, false);
                holder = new AddMore(empty_view);
                break;

        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_DATA:
                MyViewHolder itemHolder = (MyViewHolder) holder;
                initItemView(itemHolder);
                break;
            case TYPE_ADD_MORE:
                AddMore addMore = (AddMore) holder;
                initAddMoreView(addMore);
                break;
        }
    }
    private void initItemView(MyViewHolder viewHolder) {
        final int position = viewHolder.getAdapterPosition();
        final SwapPost iwantItemPojo = swapPostArrayList.get(position);
        viewHolder.exchangeItemNameTv.setText(iwantItemPojo.getSwapTitle());
        viewHolder.crossIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.iWantAdapterClick(iwantItemPojo.getSwapPostId(), position);
            }
        });
    }

    private void initAddMoreView(AddMore addMore) {
        addMore.addMoreLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.addMoreView();
            }
        });

    }



    @Override
    public int getItemViewType(int position) {
        if (swapPostArrayList.get(position).getItemType()) {
            return TYPE_ADD_MORE;
        } else {
            return TYPE_DATA;
        }
    }


    @Override
    public int getItemCount() {
        return swapPostArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView exchangeItemNameTv;
        private ImageView crossIv;

        public MyViewHolder(View itemView) {
            super(itemView);
            exchangeItemNameTv = (TextView) itemView.findViewById(R.id.exchangeItemNameTv);
            crossIv = (ImageView) itemView.findViewById(R.id.crossIv);
        }
    }

    public class AddMore extends RecyclerView.ViewHolder {
        private LinearLayout addMoreLl;
        public AddMore(View itemView) {
            super(itemView);
            addMoreLl=(LinearLayout)itemView.findViewById(R.id.addMoreLl);
        }
    }


}
