package com.twoswap.com.adapter.filter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.pojo_class.product_category.SubCategoryData;
import com.twoswap.com.utility.CircleTransform;
import com.twoswap.com.utility.ClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * <h>ProductCategoryRvAdapter</h>
 * <p>
 *     This class is called from ProductCategoryActivity class. This class extends RecyclerView.Adapter
 *     from that we have three overrided method 1. onCreateViewHolder() In this method we used to inflate
 *     single_row_product_category.  2> getItemCount() This method retuns the total number of inflated rows
 *     3>onBindViewHolder() In this method we used to set the all values to that inflated xml from list datas.
 * </p>
 * @since 2017-05-04
 */
public class FilterSubCategoryRvAdapter extends RecyclerView.Adapter<FilterSubCategoryRvAdapter.MyViewHolder>
{
    private ArrayList<SubCategoryData> aL_subCategoryDatas;
    private Activity mActivity;
    private ClickListener clickListener;
    private static final String TAG=FilterSubCategoryRvAdapter.class.getSimpleName();
    public int mSelected=-1;

    /**
     * <h>ProductCategoryRvAdapter</h>
     * <p>
     *     This is simple constructor to initailize list datas and context.
     * </p>
     * @param mActivity The current context
     * @param aL_subCategoryDatas The response datas
     */
    public FilterSubCategoryRvAdapter(Activity mActivity, ArrayList<SubCategoryData> aL_subCategoryDatas) {
        this.aL_subCategoryDatas = aL_subCategoryDatas;
        this.mActivity = mActivity;
    }

    /**
     * <h>OnCreateViewHolder</h>
     * <p>
     *     In this method The adapter prepares the layout of the items by inflating the correct
     *     layout for the individual data elements.
     * </p>
     * @param parent A ViewGroup is a special view that can contain other views (called children.)
     * @param viewType Within the getItemViewType method the recycler view determines which type should be used for data.
     * @return It returns an object of type ViewHolder per visual entry in the recycler view.
     */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(mActivity).inflate(R.layout.single_row_filter_sub_category,parent,false);
        return new MyViewHolder(view);
    }

    /**
     * <h>OnBindViewHolder</h>
     * <p>
     *     In this method Every visible entry in a recycler view is filled with the
     *     correct data model item by the adapter. Once a data item becomes visible,
     *     the adapter assigns this data to the individual widgets which he inflated
     *     earlier.
     * </p>
     * @param holder The referece of MyViewHolder class of current class.
     * @param position The position of particular item
     */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {

        if(position==mSelected){
            holder.iV_select.setVisibility(View.VISIBLE);
        }else {
            holder.iV_select.setVisibility(View.GONE);
        }
        String categoryName=aL_subCategoryDatas.get(position).getSubCategoryName();
        System.out.println(TAG+" "+"SubCategoryName="+categoryName);
        if (categoryName!=null && !categoryName.isEmpty()) {
            categoryName=categoryName.substring(0,1).toUpperCase()+categoryName.substring(1).toLowerCase();
            holder.tV_category.setText(categoryName);
        }

        if(!aL_subCategoryDatas.get(position).getImageUrl().isEmpty() && aL_subCategoryDatas.get(position).getImageUrl()!=null) {
            Picasso.with(mActivity)
                    .load(aL_subCategoryDatas.get(position).getImageUrl())
                    .resizeDimen(R.dimen.thirty_dp,R.dimen.thirty_dp)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.iV_category);
        }
    }

    /**
     * Return the size of your dataset
     * @return the total number of rows
     */
    @Override
    public int getItemCount() {
        return aL_subCategoryDatas.size();
    }

    /**
     * <h>MyViewHolder</h>
     * <p>
     *     In this class we used to declare and assign the xml variables.
     * </p>
     */
    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tV_category;
        public ImageView iV_category,iV_select;

        MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mSelected==getAdapterPosition()) {
                        if(mSelected==0) {
                            mSelected = -1;
                            aL_subCategoryDatas.get(getAdapterPosition()).setSelectd(false);
                        }
                        else {
                            mSelected = -1 * mSelected;
                            aL_subCategoryDatas.get(getAdapterPosition()).setSelectd(false);
                        }
                    }
                    else {
                        mSelected = getAdapterPosition();
                        aL_subCategoryDatas.get(getAdapterPosition()).setSelectd(true);
                    }

                    clickListener.onItemClick(v,getAdapterPosition());
                    notifyItemRangeChanged(0,aL_subCategoryDatas.size());
                }
            });
            tV_category= (TextView) itemView.findViewById(R.id.tV_category);
            iV_category= (ImageView)itemView.findViewById(R.id.iV_category);
            iV_select = (ImageView)itemView.findViewById(R.id.iV_select);
        }
    }

    public void setOnItemClick(ClickListener listener)
    {
        clickListener=listener;
    }
}
