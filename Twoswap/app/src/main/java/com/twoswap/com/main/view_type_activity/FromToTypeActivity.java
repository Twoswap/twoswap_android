package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.VariableConstants;

public class FromToTypeActivity extends AppCompatActivity implements View.OnClickListener{

    private NotificationMessageDialog mNotificationMessageDialog;
    private RelativeLayout rL_done;
    private TextView tV_from;
    private EditText eT_fromValue,eT_toValue;
    private String fieldName;
    private TextView tV_title;
    private String title;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_to_type);

        mActivity=FromToTypeActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");

        tV_title=(TextView)findViewById(R.id.tV_title);
        tV_title.setText(title);

        tV_from=(TextView)findViewById(R.id.tV_from);

        eT_fromValue=(EditText)findViewById(R.id.eT_fromValue);
        eT_toValue=(EditText)findViewById(R.id.eT_toValue);

        rL_done = (RelativeLayout)findViewById(R.id.rL_done);
        rL_done.setOnClickListener(this);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                Intent intent=getIntent();
                intent.putExtra("selectedValue",eT_fromValue.getText().toString().trim()+","+eT_toValue.getText().toString().trim());
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
