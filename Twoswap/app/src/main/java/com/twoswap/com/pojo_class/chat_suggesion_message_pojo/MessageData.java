package com.twoswap.com.pojo_class.chat_suggesion_message_pojo;

/**
 * Created by embed on 12/7/18.
 */

public class MessageData {

    String _id,message,createdOn;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
