package com.twoswap.com.pojo_class.filter;

import java.io.Serializable;

/**
 * Created by embed on 23/5/18.
 */

/*
* This model used while applied filtered data to be send to server
* we take data while inserted in view or selected
* */

public class SendFilter implements Serializable {

    private String type, filedName, value, from, to;

    public SendFilter(String type, String filedName, String value) {
        this.type = type;
        this.filedName = filedName;
        this.value = value;
    }

    public SendFilter(String type, String filedName, String from, String to) {
        this.type = type;
        this.filedName = filedName;
        this.from = from;
        this.to = to;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
