package com.twoswap.com.pojo_class.chat_suggesion_message_pojo;

import java.util.ArrayList;

/**
 * Created by embed on 12/7/18.
 */

public class MessageMainPojo {

    String code,message;
    ArrayList<MessageData> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MessageData> getData() {
        return data;
    }

    public void setData(ArrayList<MessageData> data) {
        this.data = data;
    }
}
