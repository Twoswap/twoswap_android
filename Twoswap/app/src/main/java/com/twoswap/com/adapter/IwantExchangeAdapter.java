package com.twoswap.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.main.activity.WillingToExchangeActivity;
import com.twoswap.com.pojo_class.product_details_pojo.SwapPost;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class IwantExchangeAdapter extends RecyclerView.Adapter<IwantExchangeAdapter.MyViewHolder> {
    private ArrayList<SwapPost> iWantexchangeArrayList;
    private WillingToExchangeActivity mActivity;

    public IwantExchangeAdapter(ArrayList<SwapPost> iWantexchangeArrayList, WillingToExchangeActivity mActivity) {
        this.iWantexchangeArrayList = iWantexchangeArrayList;
        this.mActivity = mActivity;

    }

    @Override
    public IwantExchangeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_iwant_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(IwantExchangeAdapter.MyViewHolder holder, final int position) {

        final SwapPost iwantItemPojo = iWantexchangeArrayList.get(position);
        holder.exchangeItemNameTv.setText(iwantItemPojo.getSwapTitle());

        holder.crossIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.iWantAdapterClick(iwantItemPojo.getSwapPostId(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return iWantexchangeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView exchangeItemNameTv;
        private ImageView crossIv;

        public MyViewHolder(View itemView) {
            super(itemView);
            exchangeItemNameTv = (TextView) itemView.findViewById(R.id.exchangeItemNameTv);
            crossIv = (ImageView) itemView.findViewById(R.id.crossIv);
        }
    }
}
