package com.twoswap.com.main.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.suresh.innapp_purches.InAppConstants;
import com.suresh.innapp_purches.Inn_App_billing.BillingProcessor;
import com.suresh.innapp_purches.Inn_App_billing.SkuDetails;
import com.suresh.innapp_purches.Inn_App_billing.TransactionDetails;
import com.twoswap.com.BusEventManager.FutureUpdated;
import com.twoswap.com.R;
import com.twoswap.com.adapter.PromoteItemRvAdapter;
import com.twoswap.com.aleret.CircleProgressDialog;
import com.twoswap.com.mqttchat.AppController;
import com.twoswap.com.pojo_class.promote_item_pojo.PromoteItemData;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.SessionManager;
import com.twoswap.com.utility.SukoCallBack;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * <h>PromoteItemActivity</h>
 * <p>
 *     In this class we used to show the list of promote item.
 * </p>
 * @since 30-Aug-17
 */
public class PromoteItemActivity extends AppCompatActivity implements View.OnClickListener,BillingProcessor.IBillingHandler
{
    private static final String TAG = PromoteItemActivity.class.getSimpleName();
    private Activity mActivity;
    private SessionManager mSessionManager;
    private RelativeLayout rL_rootElement;
    private ProgressBar mProgressBar;
    private RecyclerView rV_promotePlan;
    private static BillingProcessor bp;
    private ProgressDialog progressDialog;
    private InAppConstants.Purchase_item mpurchase_item;
    private ArrayList<PromoteItemData> arrayListPromoteItem;
    private Dialog circleDialog;
    private String postId;
    private String uniqueView;
    private String title;
    private int isPromoted;
    private RelativeLayout rl_apply;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promote_item);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        initVariables();
    }

    private void initVariables()
    {
        circleDialog=CircleProgressDialog.getInstance().get_Circle_Progress_bar(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Wait..");
        // receiving datas from last class
        String price,productName,productImage;
        Intent intent = getIntent();
        price=intent.getStringExtra("price");
        productName=intent.getStringExtra("productName");
        productImage=intent.getStringExtra("productImage");
        postId =intent.getStringExtra("productId");
        isPromoted=intent.getIntExtra("isPromoted",0);
        mActivity = PromoteItemActivity.this;
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mSessionManager = new SessionManager(mActivity);
        rL_rootElement= (RelativeLayout) findViewById(R.id.rL_rootElement);
        rV_promotePlan= (RecyclerView) findViewById(R.id.rV_promotePlan);
        rl_apply=findViewById(R.id.rL_apply);
        rl_apply.setOnClickListener(this);

        if(isPromoted==1)
            rl_apply.setVisibility(View.GONE);
        else
            rl_apply.setVisibility(View.VISIBLE);

        bp=new BillingProcessor(this, InAppConstants.base64EncodedPublicKey,mSessionManager.getUserId(),this);

        // set product desc like image, name and price
        ImageView iV_productImage= (ImageView) findViewById(R.id.iV_productImage);
        if (productImage!=null && !productImage.isEmpty())
            Picasso.with(mActivity)
                    .load(productImage)
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(iV_productImage);

        // set price
        TextView tV_productprice= (TextView) findViewById(R.id.tV_productprice);
        if (price!=null && !price.isEmpty())
            tV_productprice.setText(price);

        // set name
        TextView tV_productName= (TextView) findViewById(R.id.tV_productName);
        if (productName!=null && !productName.isEmpty())
            tV_productName.setText(productName);

        // Back button
        RelativeLayout rL_back_btn= (RelativeLayout) findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);

        promotionDetail();
       // promotionPlansApi();
    }

    public void promotionDetail(){
        AppController controller= (AppController) getApplicationContext();
        List<SkuDetails> promotionItemList=controller.getInappItemList();
        if(promotionItemList==null){
            controller.collectIN_AppDetails(new SukoCallBack() {
                @Override
                public void onSucess(List<SkuDetails> data)
                {
                    updateUi(data);
                }

                @Override
                public void onError(String message) {

                }
            });
        }
        else {
            updateUi(promotionItemList);
        }
    }

    private void updateUi(List<SkuDetails> data)
    {
        arrayListPromoteItem =new ArrayList<>();
        for(SkuDetails temp_data:data)
        {
            PromoteItemData temp=new PromoteItemData();
            temp.setPlanId(temp_data.productId);
            temp.setPrice(""+temp_data.priceText);
            temp.setName(temp_data.title);
            temp.setUniqueViews(temp_data.description);
            arrayListPromoteItem.add(temp);
        }
        if (arrayListPromoteItem!=null && arrayListPromoteItem.size()>0)
        {
            PromoteItemRvAdapter promoteItemRvAdapter = new PromoteItemRvAdapter(mActivity,arrayListPromoteItem);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
            rV_promotePlan.setLayoutManager(linearLayoutManager);
            rV_promotePlan.setAdapter(promoteItemRvAdapter);
        }

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rL_back_btn :
                onBackPressed();
                break;
            case R.id.rL_apply:
                collectSeletedItem();
                break;
        }
    }

    /*
     *buying the data details. */
    private void buy_Item(InAppConstants.Purchase_item purchase_item)
    {
        this.mpurchase_item=purchase_item;
        progressDialog.show();
        bp=new BillingProcessor(this, InAppConstants.base64EncodedPublicKey,mSessionManager.getUserId(),this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!(bp != null && bp.handleActivityResult(requestCode, resultCode, data)))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details)
    {
        Log.d("Item_purchesed", "String postId" + productId + " " + details.toString());
        if(bp.consumePurchase(productId))
        {
            purchase_Item(productId);
            bp.release();
        }
    }

    @Override
    public void onPurchaseHistoryRestored()
    {
        bp.release();
    }

    @Override
    public void onBillingError(int errorCode, Throwable error)
    {
        Toast.makeText(this, R.string.purchaset_error,Toast.LENGTH_SHORT).show();
        /*
         * Releasing the service.*/
        bp.release();
    }
    @Override
    public void onBillingInitialized()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
        }
        if(mpurchase_item!=null) {
            /*
            *For testing purpose only.*/
            bp.purchase(this, mpurchase_item.getKey());
        }
    }
    /*
     * current selected item*/
    private void collectSeletedItem()
    {
        String planId="";
        for(PromoteItemData data:arrayListPromoteItem)
        {
            if(data.isItemSelected())
            {
                planId=data.getPlanId();

                if(data.getPlanId().equalsIgnoreCase("android.test.purchased"))
                    uniqueView="100";
                else
                    uniqueView=data.getUniqueViews();

                title=data.getName();
                //Toast.makeText(mActivity,uniqueView,Toast.LENGTH_SHORT).show();
                break;
            }
        }
        if(planId.isEmpty())
        {
            Toast.makeText(this, R.string.Error_on_plandetaisl,Toast.LENGTH_SHORT).show();
            return;
        }
        /*
         *buying the item. */
        buy_Item(InAppConstants.Purchase_item.getPurchaseItem(planId));
    }
    /*
     *Purchase item.*/
    private void purchase_Item(String playItemId)
    {
        InAppConstants.Purchase_item item=InAppConstants.Purchase_item.getPlanId(playItemId);
        if(item==null)
        {
            Toast.makeText(this, R.string.Stored_item_error,Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject request_datas = new JSONObject();
        try {
            request_datas.put("token", mSessionManager.getAuthToken());
            request_datas.put("postId",postId);
            request_datas.put("postType","0");
            request_datas.put("purchaseId",item.getId());
            request_datas.put("noOfViews",uniqueView);
            request_datas.put("promotionTitle",title);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        if (CommonClass.isNetworkAvailable(mActivity)) {
            circleDialog.show();
            String promotionPlansUrl = ApiUrl.PURCHASED_PLAN;//+"/"+item.getId()+"/"+ postId;
            OkHttp3Connection.doOkHttp3Connection(TAG, promotionPlansUrl, OkHttp3Connection.Request_type.POST, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag)
                {
                    if(circleDialog!=null)
                    {
                        circleDialog.dismiss();
                    }
                    try
                    {
                        parseResult(result);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        CommonClass.showSnackbarMessage(rL_rootElement,getString(R.string.parse_exception_text));
                    }
                }
                @Override
                public void onError(String error, String user_tag)
                {
                    if(circleDialog!=null)
                    {
                        circleDialog.dismiss();
                    }
                    CommonClass.showSnackbarMessage(rL_rootElement,error);
                }
            });
        } else
        {
            CommonClass.showSnackbarMessage(rL_rootElement,getResources().getString(R.string.NoInternetAccess));
        }
    }

    /*
     *Parsing the result data.*/
    public void parseResult(String response) throws JSONException
    {
        JSONObject jsonObject=new JSONObject(response);
        String code=jsonObject.getString("code");
        if(code.equals("200"))
        {
            isPromoted=1;
            AppController.getBus().post(new FutureUpdated(postId,true));
            Intent intent = new Intent(mActivity, HomePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mActivity.startActivity(intent);
            //PromoteItemActivity.this.finish();
        }else
        {
            String message=jsonObject.getString("message");
            CommonClass.showSnackbarMessage(rL_rootElement,message);
        }
    }

}
