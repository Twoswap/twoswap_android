package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.VariableConstants;

import java.util.Calendar;

public class DatePickerActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationMessageDialog mNotificationMessageDialog;
    public TextView tV_date,tV_title;
    private ImageView iV_datePicker;
    private int mYear,mMonth,mDay;
    private String title;
    private String type;
    private String selectedValue="";
    private String fieldName;
    private RelativeLayout rL_done;
    private boolean isPick=false;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker);

        mActivity=DatePickerActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);


        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");

        if(getIntent().hasExtra("isNumber")){
            type=getIntent().getStringExtra("isNumber");
        }

        if(getIntent().hasExtra("selectedValue")){
            selectedValue=getIntent().getStringExtra("selectedValue");
        }

        tV_title = (TextView) findViewById(R.id.tV_title);
        if(title!=null && !title.isEmpty())
            tV_title.setText(title);

        tV_date = (TextView) findViewById(R.id.tV_date);
        if(selectedValue!=null && !selectedValue.isEmpty())
            tV_date.setText(selectedValue);

        iV_datePicker = (ImageView) findViewById(R.id.iV_datePicker);
        iV_datePicker.setOnClickListener(this);

        rL_done = (RelativeLayout)findViewById(R.id.rL_done);
        rL_done.setOnClickListener(this);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iV_datePicker:
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                tV_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                isPick=true;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                if(isPick) {
                    Intent intent = getIntent();
                    intent.putExtra("selectedValue", tV_date.getText().toString());
                    setResult(VariableConstants.SELECT_VALUE_REQ_CODE, intent);
                    onBackPressed();
                }else {
                    Snackbar.make(rL_done,"Please select First !", Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
