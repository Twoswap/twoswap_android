package com.twoswap.com.adapter.view_type_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.pojo_class.product_category.FilterData;
import com.twoswap.com.utility.ClickListener;

import java.util.ArrayList;

/**
 * Created by embed on 17/5/18.
 */

public class AddDetailAdap extends RecyclerView.Adapter<AddDetailAdap.MyViewHolder> {

    private Context mContext;
    private ArrayList<FilterData> data;
    private ClickListener clickListener;
    public AddDetailAdap(Context mContext, ArrayList<FilterData> data) {
        this.mContext = mContext;
        this.data = data;
        this.clickListener = (ClickListener) mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view= LayoutInflater.from(mContext).inflate(R.layout.single_row_add_detail_field_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(data.get(position).isMandatory()){
            holder.tV_mendatory.setVisibility(View.VISIBLE);
        }else {
            holder.tV_mendatory.setVisibility(View.GONE);
        }

        if(data.get(position).isSelected()){
            holder.tV_value.setText(data.get(position).getSelectedValues());
        }

        holder.tV_fieldName.setText(data.get(position).getFieldName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(view,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tV_fieldName,tV_value,tV_mendatory;
        public MyViewHolder(View itemView) {
            super(itemView);

            tV_fieldName = (TextView)itemView.findViewById(R.id.tV_fieldName);
            tV_value = (TextView)itemView.findViewById(R.id.tV_value);
            tV_mendatory = (TextView)itemView.findViewById(R.id.tV_mendatory);
        }
    }


}
