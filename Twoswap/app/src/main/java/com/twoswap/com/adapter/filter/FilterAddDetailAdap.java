package com.twoswap.com.adapter.filter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.filter_view_holder.FilterEditTextViewHolder;
import com.twoswap.com.filter_view_holder.FilterFromToViewHolder;
import com.twoswap.com.filter_view_holder.MultipleSelectionViewHolder;
import com.twoswap.com.main.activity.FilterActivity;
import com.twoswap.com.pojo_class.filter.SendFilter;
import com.twoswap.com.pojo_class.product_category.FilterData;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.CommonClass;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by embed on 17/5/18.
 */

public class FilterAddDetailAdap extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private Activity mActivity;
    private ArrayList<FilterData> data;
    private ClickListener clickListener;
    private static final int VIEW_TEXBOX=1;
    private static final int VIEW_FROM_TO=2;
    private static final int VIEW_MULTIPLE=3;
    private FilterMultipleSelectionAdap multipleSelectionAdap;
    private static final String TAG=FilterAddDetailAdap.class.getSimpleName();

    public FilterAddDetailAdap(Context mContext, ArrayList<FilterData> data) {
        this.mContext = mContext;
        this.mActivity = (Activity) mContext;
        this.data = data;
        this.clickListener = (ClickListener) mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case VIEW_TEXBOX:
                view= LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_edit_text,parent,false);
                return new FilterEditTextViewHolder(view);
            case VIEW_MULTIPLE:
                view= LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_multiple_selection,parent,false);
                return new MultipleSelectionViewHolder(view);
            case VIEW_FROM_TO:
                view= LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_from_to,parent,false);
                return new FilterFromToViewHolder(view);
            default:
                view= LayoutInflater.from(mContext).inflate(R.layout.filter_view_type_edit_text,parent,false);
                return new FilterEditTextViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (getItemViewType(position)){
            case VIEW_TEXBOX:
                final FilterEditTextViewHolder eTHolder= (FilterEditTextViewHolder) holder;
                String name1=CommonClass.getFirstCaps(data.get(position).getFieldName());
                eTHolder.tV_fieldName.setText(name1);
                //eTHolder.eT_fieldValue.setHint(data.get(position).getFieldName());

                // if already value in sendFilter list of filterActivity then set
                for(SendFilter s:((FilterActivity)mContext).sendFilters){
                    if(s.getFiledName().equals(data.get(position).getFieldName())) {
                        eTHolder.eT_fieldValue.setText(s.getValue());
                    }
                }


                eTHolder.eT_fieldValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        addValueSendFilterList(eTHolder.eT_fieldValue.getText().toString().trim(),eTHolder.getAdapterPosition());
                    }
                });
                break;
            case VIEW_MULTIPLE:
                MultipleSelectionViewHolder msHolder= (MultipleSelectionViewHolder) holder;
                String name2=CommonClass.getFirstCaps(data.get(position).getFieldName());
                msHolder.tV_fieldName.setText(name2);
                multipleSelectionAdap=new FilterMultipleSelectionAdap(mContext,getArrayListFromValues(data.get(position).getValues()),data.get(position).getFieldName());

                // if already selcted data in sendFilter list of filterActivity then make it selected
                for(SendFilter s:((FilterActivity)mContext).sendFilters){
                    if(s.getFiledName().equals(data.get(position).getFieldName())) {
                        multipleSelectionAdap.selectedData.add(s.getValue());
                    }
                }

                msHolder.rV_multiple.setLayoutManager(new LinearLayoutManager(mContext));
                msHolder.rV_multiple.setAdapter(multipleSelectionAdap);

                break;
            case VIEW_FROM_TO:
                final FilterFromToViewHolder fromToHolder= (FilterFromToViewHolder) holder;
                String name3=CommonClass.getFirstCaps(data.get(position).getFieldName());
                fromToHolder.tV_fieldName.setText(name3);

                // if already value in sendFilter list of filterActivity then set
                for(SendFilter s:((FilterActivity)mContext).sendFilters){
                    if(s.getFiledName().equals(data.get(position).getFieldName())) {
                        fromToHolder.eT_fromValue.setText(s.getFrom());
                        fromToHolder.eT_toValue.setText(s.getTo());
                    }
                }


                fromToHolder.eT_fromValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        addFromValueInSendFilter(fromToHolder.eT_fromValue.getText().toString().trim(),fromToHolder.getAdapterPosition());
                    }
                });

                fromToHolder.eT_toValue.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        addToValueInSendFilter(fromToHolder.eT_toValue.getText().toString().trim(),fromToHolder.getAdapterPosition());
                    }
                });
                break;
            default:
                break;
        }

    }

    /**
     * 1 : textbox
     * 2 : checkbox
     * 3 : slider
     * 4 : radio button
     * 5 : range
     * 6 : drop down
     * 7 : date
     * */
    @Override
    public int getItemViewType(int position) {
        switch (data.get(position).getType()){
            case 1:
                return VIEW_TEXBOX;
            case 2:
                return VIEW_MULTIPLE;
            case 3:
                return VIEW_FROM_TO;
            case 4:
                return VIEW_MULTIPLE;
            case 5:
                return VIEW_FROM_TO;
            case 6:
                return VIEW_MULTIPLE;
            case 7:
                return VIEW_TEXBOX;
            default:
                return VIEW_TEXBOX;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tV_fieldName;
        private EditText eT_fieldValue;
        public MyViewHolder(View itemView) {
            super(itemView);
            tV_fieldName = (TextView)itemView.findViewById(R.id.tV_fieldName);
            eT_fieldValue = (EditText)itemView.findViewById(R.id.eT_fieldValue);
        }
    }

    // split the values from values tag in list
    private ArrayList<String> getArrayListFromValues(String values){
        ArrayList<String> arrayList=new ArrayList<>();

        String[] val=values.split(",");

        arrayList.addAll(Arrays.asList(val));

        return arrayList;
    }

    private void addValueSendFilterList(String value, int pos){
        //if added is become true
        boolean added=false;

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if(((FilterActivity)mContext).sendFilters.size()>0){
            for (int i=0;i<((FilterActivity)mContext).sendFilters.size();i++){
                if(((FilterActivity)mContext).sendFilters.get(i).getFiledName().equalsIgnoreCase(data.get(pos).getFieldName())){
                    ((FilterActivity)mContext).sendFilters.get(i).setValue(value);
                    added=true;
                }
            }
        }
        else {
            added=true;
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("equlTo",data.get(pos).getFieldName(),value));
        }

        // if list not contain same key then add new
        if(!added){
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("equlTo",data.get(pos).getFieldName(),value));
        }

        //print to check
        for(SendFilter f:((FilterActivity)mContext).sendFilters){
            System.out.println("entered values="+f.getFiledName()+","+f.getValue());
        }
    }

    private void addFromValueInSendFilter(String value, int pos){
        //if added is become true
        boolean added=false;

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if(((FilterActivity)mContext).sendFilters.size()>0){
            for (int i=0;i<((FilterActivity)mContext).sendFilters.size();i++){
                if(((FilterActivity)mContext).sendFilters.get(i).getFiledName().equalsIgnoreCase(data.get(pos).getFieldName())){
                    ((FilterActivity)mContext).sendFilters.get(i).setFrom(value);
                    added=true;
                }
            }
        }
        else {
            added=true;
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("range",data.get(pos).getFieldName(),value,""));
        }

        // if list not contain same key then add new
        if(!added){
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("range",data.get(pos).getFieldName(),value,""));
        }

        //print to check
        for(SendFilter f:((FilterActivity)mContext).sendFilters){
            System.out.println("from to values = "+f.getFiledName()+":"+f.getFrom()+","+f.getTo());
        }
    }

    private void addToValueInSendFilter(String value, int pos){
        //if added is become true
        boolean added=false;

        // list is empty then add new
        // else check is contains same key then added values to perticular key
        if(((FilterActivity)mContext).sendFilters.size()>0){
            for (int i=0;i<((FilterActivity)mContext).sendFilters.size();i++){
                if(((FilterActivity)mContext).sendFilters.get(i).getFiledName().equalsIgnoreCase(data.get(pos).getFieldName())){
                    ((FilterActivity)mContext).sendFilters.get(i).setTo(value);
                    added=true;
                }
            }
        }
        else {
            added=true;
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("range",data.get(pos).getFieldName(),"",value));
        }

        // if list not contain same key then add new
        if(!added){
            ((FilterActivity)mContext).sendFilters.add(new SendFilter("range",data.get(pos).getFieldName(),"",value));
        }

        //print to check
        for(SendFilter f:((FilterActivity)mContext).sendFilters){
            System.out.println("from to values = "+f.getFiledName()+":"+f.getFrom()+","+f.getTo());
        }
    }
}
