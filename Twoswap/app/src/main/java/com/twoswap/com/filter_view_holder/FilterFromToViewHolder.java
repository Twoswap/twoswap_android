package com.twoswap.com.filter_view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.twoswap.com.R;

/**
 * Created by embed on 22/5/18.
 */

public class FilterFromToViewHolder extends RecyclerView.ViewHolder {
    public EditText eT_fromValue, eT_toValue;
    public TextView tV_fieldName;
    public FilterFromToViewHolder(View itemView) {
        super(itemView);
        tV_fieldName=(TextView)itemView.findViewById(R.id.tV_fieldName);
        eT_toValue = (EditText)itemView.findViewById(R.id.eT_toValue);
        eT_fromValue = (EditText)itemView.findViewById(R.id.eT_fromValue);
    }
}
