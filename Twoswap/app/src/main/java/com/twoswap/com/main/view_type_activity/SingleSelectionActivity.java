package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.view_type_adapter.SingleSelectionAdap;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.VariableConstants;

import java.util.ArrayList;
import java.util.Arrays;

public class SingleSelectionActivity extends AppCompatActivity implements ClickListener,View.OnClickListener {

    private NotificationMessageDialog mNotificationMessageDialog;
    private RecyclerView rV_singleSelection;
    private ArrayList<String> arrayList;
    private SingleSelectionAdap singleSelectionAdap;
    private RelativeLayout rL_done;
    private String values,fieldName,title;
    private TextView tV_title;
    private String selectedValue="";
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_selection);

        mActivity=SingleSelectionActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        arrayList =new ArrayList<>();
        values=getIntent().getStringExtra("values");
        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");
        selectedValue=getIntent().getStringExtra("selectedValue");

        arrayList=getArrayListFromValues(values);

        tV_title=(TextView)findViewById(R.id.tV_title);
        tV_title.setText(title);

        rV_singleSelection=(RecyclerView)findViewById(R.id.rV_singleSelection);
        singleSelectionAdap = new SingleSelectionAdap(this,arrayList);
        rV_singleSelection.setLayoutManager(new LinearLayoutManager(this));

        if(selectedValue!=null && !selectedValue.isEmpty()){
            for(int i=0;i<arrayList.size();i++){
                if(selectedValue.equalsIgnoreCase(arrayList.get(i))){
                    singleSelectionAdap.mSelected=i;
                }
            }
        }

        rV_singleSelection.setAdapter(singleSelectionAdap);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
        rL_done=(RelativeLayout)findViewById(R.id.rL_done);
        //rL_done.setOnClickListener(this);

    }

    @Override
    public void onItemClick(View view, int position) {

        if(singleSelectionAdap.mSelected>=0){
            Intent intent=getIntent();
            intent.putExtra("selectedValue",arrayList.get(singleSelectionAdap.mSelected));
            setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
            onBackPressed();
            //rL_done.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                Intent intent=getIntent();
                intent.putExtra("selectedValue",arrayList.get(singleSelectionAdap.mSelected));
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
                onBackPressed();
                break;
        }
    }

    public ArrayList<String> getArrayListFromValues(String values){
        ArrayList<String> arrayList=new ArrayList<>();

        String[] val=values.split(",");

        arrayList.addAll(Arrays.asList(val));

        return arrayList;
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
