package com.twoswap.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.FilterCategoryRvAdapter;
import com.twoswap.com.adapter.filter.FilterAddDetailAdap;
import com.twoswap.com.adapter.filter.FilterSubCategoryRvAdapter;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.get_current_location.FusedLocationReceiver;
import com.twoswap.com.get_current_location.FusedLocationService;
import com.twoswap.com.pojo_class.filter.SendFilter;
import com.twoswap.com.pojo_class.product_category.FilterData;
import com.twoswap.com.pojo_class.product_category.FilterKeyValue;
import com.twoswap.com.pojo_class.product_category.ProductCategoryResDatas;
import com.twoswap.com.pojo_class.product_category.SubCategoryData;
import com.twoswap.com.pojo_class.product_category.SubCategoryMainPojo;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.GetCompleteAddress;
import com.twoswap.com.utility.ItemDecorationRvItems;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.ProductItemClickListener;
import com.twoswap.com.utility.RunTimePermission;
import com.twoswap.com.utility.SessionManager;
import com.twoswap.com.utility.VariableConstants;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * <h>FilterActivity</h>
 * <p>
 *     This class is called from HomePageFrag class. In this class we used to save the selected
 *     filter datas like address, category etc and send back to the HomePageFrag class.
 * </p>
 * @since 4/17/2017
 * @author 3Embed
 * @version 1.0
 */
public class FilterActivity extends AppCompatActivity implements View.OnClickListener,ProductItemClickListener,ClickListener {
    private static final String TAG = FilterActivity.class.getSimpleName();
    private SeekBar distance_seekbar;
    private RadioButton radio_NewestFirst, radio_ClosestFirst, radio_highToLow, radio_lowToHigh, radio_last24hr, radio_last7day, radio_last30day, radio_allProduct;
    private FusedLocationService locationService;
    private String[] permissionsArray;
    private RunTimePermission runTimePermission;
    private Activity mActivity;
    private ProgressBar progress_bar_save;
    private RelativeLayout rL_rootElement;
    private TextView tV_currency,tV_setLocation,tV_starting_dis, tV_save;
    private ImageView iV_dropDown_currency;
    private FilterCategoryRvAdapter categoryRvAdapter;
    private EditText eT_minprice,eT_maxprice;
    private String currency_symbol="",currency_code="",userLat="",userLng="",sortByText="",postedWithinText="",address="",sortBy="",setDistanceValue="",postedWithin="",minPrice="",maxPrice="";
    private ArrayList<ProductCategoryResDatas> aL_categoryDatas;
    private boolean isToApplyFilter;
    private NotificationMessageDialog mNotificationMessageDialog;
    private SessionManager mSessionManager;
    private boolean isToStartActivity;
    public String event="",housing="";
    public String categoryName="";
    private String subCategoryName;
    public ArrayList<FilterKeyValue> filterKeyValues=new ArrayList<>();
    private LinearLayout linear_filterField,linear_subCategory;
    private RecyclerView rV_subCategory,rV_filterField;
    private FilterSubCategoryRvAdapter filtersubCategoryRvAdapter;
    private ArrayList<SubCategoryData> aL_subCategoryDatas;
    private ArrayList<FilterData> filterData;
    private FilterAddDetailAdap filterAddDetailAdap;

    //used for send filter array
    public ArrayList<SendFilter> sendFilters=new ArrayList<>();
    private boolean isShow=false;
    private ProgressBar progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        overridePendingTransition(R.anim.slide_up, R.anim.stay);

        initializeVariables();
    }

    /**
     * <h>InitializeVariables</h>
     * <p>
     * In this method we used to initialize The data member
     * and xml all variable.
     * </p>
     */
    private void initializeVariables()
    {
        mActivity = FilterActivity.this;
        mSessionManager=new SessionManager(mActivity);
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        isToApplyFilter=false;
        isToStartActivity = true;

        // Receive datas from HomePage Frag
        Intent intent=getIntent();
        aL_categoryDatas= (ArrayList<ProductCategoryResDatas>) intent.getSerializableExtra("aL_categoryDatas");
        address=intent.getStringExtra("address");
        setDistanceValue = intent.getStringExtra("distance");
        sortBy=intent.getStringExtra("sortBy");
        postedWithin=intent.getStringExtra("postedWithin");
        currency_code=intent.getStringExtra("currency_code");
        currency_symbol=intent.getStringExtra("currency_symbol");
        minPrice=intent.getStringExtra("minPrice");
        maxPrice=intent.getStringExtra("maxPrice");
        userLat=intent.getStringExtra("userLat");
        userLng=intent.getStringExtra("userLng");
        subCategoryName=intent.getStringExtra("subCategoryName");
        sendFilters= (ArrayList<SendFilter>) intent.getSerializableExtra("sendFilters");

        permissionsArray = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        runTimePermission = new RunTimePermission(mActivity, permissionsArray, false);

        //center progress bar

        progress_bar=(ProgressBar)findViewById(R.id.progress_bar);

        //sub category list
        linear_subCategory=(LinearLayout)findViewById(R.id.linear_subCategory);
        rV_subCategory=(RecyclerView)findViewById(R.id.rV_subCategory);
        rV_subCategory.setNestedScrollingEnabled(false);
        rV_subCategory.setLayoutManager(new LinearLayoutManager(this));

        //add detail list
        linear_filterField=(LinearLayout)findViewById(R.id.linear_filterField);
        rV_filterField=(RecyclerView)findViewById(R.id.rV_filterField);
        rV_filterField.setNestedScrollingEnabled(false);
        rV_filterField.setLayoutManager(new LinearLayoutManager(this));

        // set category recyclerview adapter
        RecyclerView rV_category = (RecyclerView) findViewById(R.id.rV_category);
        categoryRvAdapter = new FilterCategoryRvAdapter(mActivity, aL_categoryDatas,this);
        GridLayoutManager layoutManager = new GridLayoutManager(mActivity,3);
        // Set spacing the recycler view items
        int spanCount = 3; // 5 columns
        int spacing = 20; // 200px
        rV_category.addItemDecoration(new ItemDecorationRvItems(spanCount, spacing,true));
        rV_category.setLayoutManager(layoutManager);
        rV_category.setAdapter(categoryRvAdapter);

        // Initialize xml variables
        CommonClass.statusBarColor(mActivity);
        rL_rootElement = (RelativeLayout) findViewById(R.id.rL_rootElement);
        tV_save = (TextView) findViewById(R.id.tV_save);
        TextView tV_reset = (TextView) findViewById(R.id.tV_reset);
        tV_reset.setOnClickListener(this);
        progress_bar_save = (ProgressBar) findViewById(R.id.progress_bar_save);
        progress_bar_save.setVisibility(View.GONE);
        RelativeLayout rL_save, rL_back_btn;
        rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
        rL_save = (RelativeLayout) findViewById(R.id.rL_apply);
        rL_save.setOnClickListener(this);
        eT_minprice= (EditText) findViewById(R.id.eT_minprice);
        eT_maxprice= (EditText) findViewById(R.id.eT_maxprice);

        // Distance seekbar
        tV_starting_dis = (TextView) findViewById(R.id.tV_starting_dis);
        distance_seekbar = (SeekBar) findViewById(R.id.distance_seekbar);
        distance_seekbar.setMax(3000);
        distance_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String setValue = progress + " " + getResources().getString(R.string.km);
                tV_starting_dis.setText(setValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        // sorted by value
        radio_NewestFirst = (RadioButton) findViewById(R.id.radio_NewestFirst);
        radio_ClosestFirst = (RadioButton) findViewById(R.id.radio_ClosestFirst);
        radio_highToLow = (RadioButton) findViewById(R.id.radio_highToLow);
        radio_lowToHigh = (RadioButton) findViewById(R.id.radio_lowToHigh);

        // posted within
        radio_last24hr = (RadioButton) findViewById(R.id.radio_last24hr);
        radio_last7day = (RadioButton) findViewById(R.id.radio_last7day);
        radio_last30day = (RadioButton) findViewById(R.id.radio_last30day);
        radio_allProduct = (RadioButton) findViewById(R.id.radio_allProduct);

        // Currency
        RelativeLayout rL_cuurency = (RelativeLayout) findViewById(R.id.rL_currency);
        rL_cuurency.setOnClickListener(this);
        tV_currency= (TextView) findViewById(R.id.tV_currency);
        tV_setLocation= (TextView) findViewById(R.id.tV_setLocation);
        iV_dropDown_currency= (ImageView) findViewById(R.id.iV_dropDown_currency);

        setDefaultCurrency();

        // Change location
        RelativeLayout rL_changeLoc= (RelativeLayout) findViewById(R.id.rL_changeLoc);
        rL_changeLoc.setOnClickListener(this);

        if (runTimePermission.checkPermissions(permissionsArray)) {
            if (!isLocationFound(userLat,userLng)) {
                System.out.println(TAG+" "+"is location found="+isLocationFound(userLat,userLng));
                isToApplyFilter = false;
                getCurrentLocation();
            }
        } else {
            runTimePermission.requestPermission();
        }
        setFilterDatas();

        //intialize fragments of categories
        /*eventFrag = new EventFrag();
        housingFrag = new HousingFrag();
        textbookFrag = new TextbookFrag();
        transportationFrag = new TransportationFrag();
        tutorFrag = new TutorFrag();*/

    }

    /**
     * <h>setFilterDatas</h>
     * <p>
     *     In this method we used set the xml variables
     * </p>
     */
    private void setFilterDatas()
    {
        // set address
        if (address!=null && !address.isEmpty())
            tV_setLocation.setText(address);

        // set distance
        if (setDistanceValue!=null && !setDistanceValue.isEmpty()) {
            distance_seekbar.setProgress(Integer.parseInt(setDistanceValue));
            setDistanceValue = setDistanceValue + " " + getResources().getString(R.string.km);
            tV_starting_dis.setText(setDistanceValue);
        }

        System.out.println(TAG+" "+"sortBy value="+sortBy);
        // set sort by radio button
        switch (sortBy)
        {
            // Newest first
            case "postedOnDesc" :
                radio_NewestFirst.setChecked(true);
                break;

            // closest first
            case "distanceAsc" :
                radio_ClosestFirst.setChecked(true);
                break;

            // High to low
            case "priceDsc" :
                radio_highToLow.setChecked(true);
                break;

            // low to high
            case "priceAsc" :
                radio_lowToHigh.setChecked(true);
                break;
        }

        // set posted within
        switch (postedWithin)
        {
            // The last 24 hour
            case "1" :
                radio_last24hr.setChecked(true);
                break;

            // Last 7 days
            case "7" :
                radio_last7day.setChecked(true);
                break;

            // The last 30 days
            case "30" :
                radio_last30day.setChecked(true);
                break;
        }

        // set currency code
        if (currency_code!=null && !currency_code.isEmpty())
            tV_currency.setText(currency_code);

        // min price
        if (minPrice!=null && !minPrice.isEmpty())
            eT_minprice.setText(minPrice);

        if (maxPrice!=null && !maxPrice.isEmpty())
            eT_maxprice.setText(maxPrice);
    }

    /**
     * <h>SetDefaultCurrency</h>
     * <p>
     *     In this method we used to find the current country currency.
     * </p>
     */
    private void setDefaultCurrency()
    {
        Map<Currency, Locale> map = CommonClass.getCurrencyLocaleMap();
        String countryIsoCode = Locale.getDefault().getCountry();
        Locale locale = new Locale("EN",countryIsoCode);
        Currency currency = Currency.getInstance(locale);
        currency_symbol = currency.getSymbol(map.get(currency));
        if (currency_symbol!=null && !currency_symbol.isEmpty())
        {
            tV_currency.setText(String.valueOf(currency));
            currency_code=String.valueOf(currency);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            // Back button
            case R.id.rL_back_btn:
                onBackPressed();
                break;

            // save filtered datas
            case R.id.rL_apply:
                double minValue=0;
                double maxValue=0;
                if (!eT_minprice.getText().toString().isEmpty())
                    minValue=Double.parseDouble(eT_minprice.getText().toString());
                if (!eT_maxprice.getText().toString().isEmpty())
                    maxValue=Double.parseDouble(eT_maxprice.getText().toString());
                if (maxValue>=minValue) {
                    if (!tV_setLocation.getText().toString().equals(getResources().getString(R.string.change_Location))) {
                        if (!isLocationFound(userLat, userLng)) {
                            if (runTimePermission.checkPermissions(permissionsArray)) {
                                isToApplyFilter=true;
                                tV_save.setVisibility(View.GONE);
                                progress_bar_save.setVisibility(View.VISIBLE);
                                getCurrentLocation();
                            } else {
                                runTimePermission.requestPermission();
                            }
                        } else {
                            getAllSavedValues();
                        }
                    } else
                        CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.please_select_your_loc));
                }

                else
                    CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.min_price_cant_be_higher));

                break;

            // reset filter
            case R.id.tV_reset :
                isToApplyFilter=false;
                getCurrentLocation();
                resetFilterDatas();
                break;

            // Currency picker
            case R.id.rL_currency:
                if (isToStartActivity) {
                    isToStartActivity = false;
                    intent = new Intent(mActivity, CurrencyListActivity.class);
                    startActivityForResult(intent, VariableConstants.CURRENCY_REQUEST_CODE);
                }
                break;

            // Change location
            case R.id.rL_changeLoc :
                if (isToStartActivity) {
                    isToStartActivity = false;
                    intent = new Intent(mActivity, ChangeLocationActivity.class);
                    startActivityForResult(intent, VariableConstants.CHANGE_LOC_REQ_CODE);
                }
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        isToStartActivity = true;
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private void getCurrentLocation() {
        locationService = new FusedLocationService(mActivity, new FusedLocationReceiver() {
            @Override
            public void onUpdateLocation() {
                Location currentLocation = locationService.receiveLocation();
                if (currentLocation != null) {
                    userLat = String.valueOf(currentLocation.getLatitude());
                    userLng= String.valueOf(currentLocation.getLongitude());
                    progress_bar_save.setVisibility(View.GONE);
                    if (isLocationFound(userLat, userLng))
                    {
                        System.out.println(TAG + " " + "lat=" + userLat + " " + "lng=" + userLng);
                        mSessionManager.setCurrentLat(userLat);
                        mSessionManager.setCurrentLng(userLng);

                        if (isToApplyFilter)
                            getAllSavedValues();
                        String address=CommonClass.getCompleteAddressString(mActivity,currentLocation.getLatitude(),currentLocation.getLongitude());
                        if (!address.isEmpty())
                            tV_setLocation.setText(address);
                        else
                            new GetCompleteAddress(mActivity,userLat,userLng,tV_setLocation,null);
                    }
                }
            }
        }
        );
    }


    /**
     * <h>GetAllSavedValues</h>
     * <p>
     * In this method we fetch all saved values like categories, distance, price etc
     * once we receive all those values we send back all those values to previous
     * activity and do filter operation.
     * </p>
     */
    private void getAllSavedValues() {
        String distance, postedWithin, minPrice = eT_minprice.getText().toString(), maxPrice = eT_maxprice.getText().toString();

        distance = distance_seekbar.getProgress()+"";
        postedWithin = getPostedWithinValue();
        getSortBy();

        if (distance.equals("0"))
            distance="";

        System.out.println(TAG+" "+"currency code="+tV_currency.getText().toString()+" "+" "+"distance="+distance+" "+"latitude="+userLat+" "+"long="+userLng+" "+"maxPrice="+maxPrice+" "+"min price="+minPrice+" "+"postedWithin="+postedWithin+" "+"sortBy="+sortBy+" "+"location="+tV_setLocation.getText().toString()+" ");

        Intent filterIntent = new Intent();
        filterIntent.putExtra("aL_categoryDatas",aL_categoryDatas);
        filterIntent.putExtra("distance", distance);
        filterIntent.putExtra("currentLatitude",userLat);
        filterIntent.putExtra("currentLongitude",userLng);
        filterIntent.putExtra("address",tV_setLocation.getText().toString());
        filterIntent.putExtra("sortBy",sortBy);
        filterIntent.putExtra("postedWithin", postedWithin);
        filterIntent.putExtra("minPrice", minPrice);
        filterIntent.putExtra("maxPrice", maxPrice);
        filterIntent.putExtra("currency_code",currency_code);
        filterIntent.putExtra("currency",currency_symbol);
        filterIntent.putExtra("postedWithinText",postedWithinText);
        filterIntent.putExtra("sortByText",sortByText);
        filterIntent.putExtra("categoryName",categoryName);
        filterIntent.putExtra("subCategoryName",subCategoryName);
        filterIntent.putExtra("sendFilters",sendFilters);
        setResult(VariableConstants.FILTER_REQUEST_CODE, filterIntent);
        progress_bar_save.setVisibility(View.GONE);
        onBackPressed();
    }

    /**
     * <h>ResetFilterDatas</h>
     * <p>
     *     In this method we used to reset all the selected filtered values.
     * </p>
     */
    private void resetFilterDatas()
    {
        // clear selected category
        if (aL_categoryDatas!=null && aL_categoryDatas.size()>0)
        {
            for (ProductCategoryResDatas productCategoryResDatas : aL_categoryDatas)
            {
                System.out.println(TAG+" "+"name="+productCategoryResDatas.getName()+" "+"selected value="+productCategoryResDatas.isSelected());
                productCategoryResDatas.setSelected(false);
            }
        }
        categoryRvAdapter.notifyDataSetChanged();

        categoryName="";
        subCategoryName="";
        sendFilters.clear();
        linear_subCategory.setVisibility(View.GONE);
        linear_filterField.setVisibility(View.GONE);

        // location
        address=getResources().getString(R.string.change_Location);
        userLat="";
        userLng="";

        // set distance
        setDistanceValue=getResources().getString(R.string.zero);

        // sort by
        sortBy="";
        radio_NewestFirst.setChecked(false);
        radio_ClosestFirst.setChecked(false);
        radio_highToLow.setChecked(false);
        radio_lowToHigh.setChecked(false);

        // posted within
        postedWithin="";
        radio_last24hr.setChecked(false);
        radio_last7day.setChecked(false);
        radio_last30day.setChecked(false);
        radio_allProduct.setChecked(false);

        // currency
        setDefaultCurrency();

        // price
        eT_minprice.getText().clear();
        eT_maxprice.getText().clear();
        maxPrice="";
        minPrice="";

        System.out.println(TAG+" "+"min price="+eT_minprice.getText().toString()+" "+"max price="+eT_maxprice.getText().toString());

        //eT_minprice.setHint(getResources().getString(R.string.default_price));
        //eT_maxprice.setHint(getResources().getString(R.string.default_price));

        setFilterDatas();
    }

    /**
     * <h>GetSortBy</h>
     * <p>
     *     In this method we used to set sort by value according to checked sort by checked radio button.
     * </p>
     */
    private void getSortBy()
    {
        if (radio_NewestFirst.isChecked())
        {
            sortBy=getResources().getString(R.string.postedOnDesc);
            sortByText=getResources().getString(R.string.newest_first);
        }
        else if (radio_ClosestFirst.isChecked())
        {
            sortBy=getResources().getString(R.string.distanceAsc);
            sortByText=getResources().getString(R.string.closest_first);
        }
        else if (radio_highToLow.isChecked())
        {
            sortBy=getResources().getString(R.string.priceDsc);
            sortByText=getResources().getString(R.string.price_high_to_low);
        }
        else if (radio_lowToHigh.isChecked())
        {
            sortBy=getResources().getString(R.string.priceAsc);
            sortByText=getResources().getString(R.string.price_low_to_high);
        }
        else sortBy="";
    }

    /**
     * <h>GetPostedWithinValue</h>
     * <p>
     * In this method we used to find any one selected Posted Within Value since
     * it contains radiobutton.
     * </p>
     *
     * @return The selected sort value
     */
    private String getPostedWithinValue() {
        String postedWithin = "";
        if (radio_last24hr.isChecked())
        {
            postedWithin = "1";                 // i.e one day
            postedWithinText=getResources().getString(R.string.the_last_24hr);
        }
        else if (radio_last7day.isChecked())
        {
            postedWithin = "2";                 // i.e seven day
            postedWithinText=getResources().getString(R.string.the_last_7day);
        }
        else if (radio_last30day.isChecked())
        {
            postedWithin = "3";                // i.e 30 day
            postedWithinText=getResources().getString(R.string.the_last_30day);
        }
        else if (radio_allProduct.isChecked())
        {
            postedWithin = "";                  // i.e by default all product
            postedWithinText=getResources().getString(R.string.all_producs);
        }
        return postedWithin;
    }

    /**
     * In this method we used to check whether current lat and
     * long has been received or not.
     *
     * @param lat The current latitude
     * @param lng The current longitude
     * @return boolean flag true or false
     */
    private boolean isLocationFound(String lat, String lng) {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case VariableConstants.PERMISSION_REQUEST_CODE:
                System.out.println("grant result=" + grantResults.length);
                if (grantResults.length > 0) {
                    for (int count = 0; count < grantResults.length; count++) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED)
                            runTimePermission.allowPermissionAlert(permissions[count]);

                    }
                    System.out.println("isAllPermissionGranted=" + runTimePermission.checkPermissions(permissionsArray));
                    if (runTimePermission.checkPermissions(permissionsArray)) {
                        getCurrentLocation();
                    }
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(TAG + " " + "on activity result called...."+" "+"req code="+requestCode+" "+"res code="+resultCode+" "+"data="+data);
        if (data != null) {
            switch (requestCode) {
                // currency symbol
                case VariableConstants.CURRENCY_REQUEST_CODE:
                    currency_code = data.getStringExtra("cuurency_code");
                    currency_symbol = data.getStringExtra("currency_symbol");

                    // set currency cde eg. Inr
                    if (currency_code != null)
                        tV_currency.setText(currency_code);

                    // change dropdown icon
                    iV_dropDown_currency.setImageResource(R.drawable.drop_down_black_color_icon);
                    break;

                case VariableConstants.CHANGE_LOC_REQ_CODE :
                    userLat=data.getStringExtra("lat");
                    userLng=data.getStringExtra("lng");
                    address=data.getStringExtra("address");
                    tV_setLocation.setText(address);
                    System.out.println(TAG+" "+"lat="+userLat+" "+"lng="+userLng+" "+"address="+address);
                    break;
            }
        }
    }


    //call back from category clicked.
    @Override
    public void onItemClick(int pos, ImageView imageView) {

        // hide while selected other category
        linear_filterField.setVisibility(View.GONE);

        if(aL_categoryDatas.get(pos).isSelected())
            categoryName=aL_categoryDatas.get(pos).getName();
        else
            categoryName="";

        sendFilters.clear();
        subCategoryName="";
        System.out.println(TAG+"selected categoryName:"+categoryName);

        if(aL_categoryDatas.get(pos).getSubCategoryCount()>0 && !categoryName.isEmpty()) {
            getSubCategoriesService();
        }
        else if(aL_categoryDatas.get(pos).getFilterCount()>0){
            setCategoryFilterData(pos);
        }
        else {
            linear_subCategory.setVisibility(View.GONE);
            linear_filterField.setVisibility(View.GONE);
        }
    }

    public void setCategoryFilterData(int pos){
        linear_filterField.setVisibility(View.VISIBLE);
        linear_subCategory.setVisibility(View.GONE);
        filterData = aL_categoryDatas.get(pos).getFilter();
        filterAddDetailAdap = new FilterAddDetailAdap(mActivity, filterData);
        rV_filterField.setAdapter(filterAddDetailAdap);
    }


    /**
     * <h>GetCategoriesService</h>
     * <p>
     *     This method is called from onCreate() method of the current class.
     *     In this method we used to call the getCategories api using okHttp3.
     *     Once we get the data we show that list in recyclerview.
     * </p>
     */
    public void getSubCategoriesService()
    {
        if (CommonClass.isNetworkAvailable(mActivity)) {

            progress_bar.setVisibility(View.VISIBLE);

            JSONObject request_datas = new JSONObject();
            String Url= ApiUrl.GET_SUB_CATEGORIES+"?categoryName="+categoryName;

            OkHttp3Connection.doOkHttp3Connection(TAG, Url, OkHttp3Connection.Request_type.GET, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {

                    System.out.println(TAG+" "+"get sub category res="+result);

                    SubCategoryMainPojo subCategoryMainPojo;
                    Gson gson=new Gson();
                    subCategoryMainPojo=gson.fromJson(result,SubCategoryMainPojo.class);

                    switch (subCategoryMainPojo.getCode())
                    {
                        // success
                        case "200" :
                            progress_bar.setVisibility(View.GONE);
                            linear_subCategory.setVisibility(View.VISIBLE);
                            aL_subCategoryDatas=subCategoryMainPojo.getData();
                            filtersubCategoryRvAdapter=new FilterSubCategoryRvAdapter(mActivity,aL_subCategoryDatas);
                            rV_subCategory.setAdapter(filtersubCategoryRvAdapter);

                            //  if already selected once
                            setSubCategoryData();

                            if (aL_subCategoryDatas!=null && aL_subCategoryDatas.size()>0)
                            {
                                filtersubCategoryRvAdapter.setOnItemClick(new ClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {

                                        sendFilters.clear();
                                        if(filtersubCategoryRvAdapter.mSelected>=0) {
                                            subCategoryName = aL_subCategoryDatas.get(position).getSubCategoryName();
                                            linear_filterField.setVisibility(View.VISIBLE);
                                        }else { //handle for unselected
                                            subCategoryName="";
                                            linear_filterField.setVisibility(View.GONE);
                                        }
                                        System.out.println(TAG+"selected subCategoryName:"+subCategoryName);
                                        if(aL_subCategoryDatas.get(position).getFieldCount()>0) {
                                            //linear_filterField.setVisibility(View.VISIBLE);
                                            filterData = aL_subCategoryDatas.get(position).getFilter();
                                            filterAddDetailAdap = new FilterAddDetailAdap(mActivity, filterData);
                                            rV_filterField.setAdapter(filterAddDetailAdap);
                                        }else {
                                           // linear_filterField.setVisibility(View.GONE);
                                        }
                                    }
                                });

                            }
                            break;

                        // Error
                        default:
                            progress_bar.setVisibility(View.GONE);
                            CommonClass.showSnackbarMessage(rL_rootElement,subCategoryMainPojo.getMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    progress_bar.setVisibility(View.GONE);
                    CommonClass.showSnackbarMessage(rL_rootElement,error);
                }
            });
        }
        else CommonClass.showSnackbarMessage(rL_rootElement,getResources().getString(R.string.NoInternetAccess));
    }

    //callback from add detail item click
    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return true;
    }

    public void setSubCategoryData(){
        if(subCategoryName!=null && !subCategoryName.isEmpty()){
            for (int i=0;i<aL_subCategoryDatas.size();i++){
                // set selected subcat name
                if(aL_subCategoryDatas.get(i).getSubCategoryName().equalsIgnoreCase(subCategoryName)) {
                    filtersubCategoryRvAdapter.mSelected = i;

                    //shows selected subcat fields
                    if(aL_subCategoryDatas.get(i).getFieldCount()>0) {
                        filterData = aL_subCategoryDatas.get(i).getFilter();
                        filterAddDetailAdap = new FilterAddDetailAdap(mActivity, filterData);
                        rV_filterField.setAdapter(filterAddDetailAdap);
                        linear_filterField.setVisibility(View.VISIBLE);
                    }
                }
            }
        }


    }
}


/*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (pos){
            case 0:
                if(eventFrag.isAdded())
                    transaction.show(eventFrag);
                else
                    transaction.add(R.id.frame_subCategories,eventFrag);

                hideOtherFrags(transaction,new Fragment[]{housingFrag,textbookFrag,transportationFrag,tutorFrag});
                break;
            case 1:
                if(housingFrag.isAdded())
                    transaction.show(housingFrag);
                else
                    transaction.add(R.id.frame_subCategories,housingFrag);

                hideOtherFrags(transaction,new Fragment[]{eventFrag,textbookFrag,transportationFrag,tutorFrag});
                break;
            case 2:
                if(textbookFrag.isAdded())
                    transaction.show(textbookFrag);
                else
                    transaction.add(R.id.frame_subCategories,textbookFrag);
                hideOtherFrags(transaction,new Fragment[]{housingFrag,eventFrag,transportationFrag,tutorFrag});
                break;
            case 3:
                if(transportationFrag.isAdded())
                    transaction.show(transportationFrag);
                else
                    transaction.add(R.id.frame_subCategories,transportationFrag);

                hideOtherFrags(transaction,new Fragment[]{housingFrag,textbookFrag,eventFrag,tutorFrag});
                break;
            case 4:
                if(tutorFrag.isAdded())
                    transaction.show(tutorFrag);
                else
                    transaction.add(R.id.frame_subCategories,tutorFrag);

                hideOtherFrags(transaction,new Fragment[]{housingFrag,textbookFrag,transportationFrag,eventFrag});
                break;
        }
        transaction.commit();*/
