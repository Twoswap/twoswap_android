package com.twoswap.com.pojo_class.product_details_pojo;

import java.io.Serializable;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class SwapPost implements Serializable {

    private String swapPostId;

    private String swapTitle;
    private boolean itemType;


    public String getSwapPostId() {
        return swapPostId;
    }

    public void setSwapPostId(String swapPostId) {
        this.swapPostId = swapPostId;
    }

    public String getSwapTitle() {
        return swapTitle;
    }

    public void setSwapTitle(String swapTitle) {
        this.swapTitle = swapTitle;
    }

    public void setItemType(boolean itemType) {
        this.itemType = itemType;
    }

    public boolean getItemType() {
        return itemType;
    }

    @Override
    public String toString() {
        return "ClassPojo [swapPostId = " + swapPostId + ", swapTitle = " + swapTitle + "itemType"+itemType+"]";
    }
}
