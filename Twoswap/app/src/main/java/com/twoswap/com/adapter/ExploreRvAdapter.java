package com.twoswap.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdIconView;

import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.squareup.picasso.Picasso;
import com.twoswap.com.R;
import com.twoswap.com.pojo_class.home_explore_pojo.ExploreResponseDatas;
import com.twoswap.com.pojo_class.product_details_pojo.SwapPost;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.ProductItemClickListener;
import java.util.ArrayList;
import java.util.List;


/**
 * <h>ExploreRvAdapter</h>
 * <p>
 *     In class is called from ExploreFrag. In this recyclerview adapter class we used to inflate
 *     single_row_images layout and shows the all post posted by logged-in user.
 * </p>
 * @since 4/6/2017
 */
public class ExploreRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final String TAG = ExploreRvAdapter.class.getSimpleName();
    private Activity mActivity;
    private ArrayList<ExploreResponseDatas> arrayListExploreDatas;
    private final ProductItemClickListener animalItemClickListener;
    private static final int AD_VIEW=1;
    private static final int ITEM_VIEW=0;

    public ExploreRvAdapter(Activity mActivity, ArrayList<ExploreResponseDatas> arrayListExploreDatas,ProductItemClickListener animalItemClickListener) {
        this.mActivity = mActivity;
        this.arrayListExploreDatas = arrayListExploreDatas;
        this.animalItemClickListener=animalItemClickListener;

        System.out.println(TAG+" "+"al size in adap="+arrayListExploreDatas);

        WindowManager wm = (WindowManager)mActivity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if(viewType==AD_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.facebook_native_ad_item, parent, false);
            return new NativeAdViewHolder(view);
        }else {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_explore, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position)
    {
        if(getItemViewType(position)==AD_VIEW){

            //....AdMob Native Ad

          /*  AdViewHolder adViewHolder= (AdViewHolder) holder;

            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) adViewHolder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            UnifiedNativeAdView adView = (UnifiedNativeAdView) mActivity.getLayoutInflater()
                    .inflate(R.layout.ad_unified, null);
            if(arrayListExploreDatas.get(position).getUnifiedNativeAd()!=null) {
                populateUnifiedNativeAdView(arrayListExploreDatas.get(position).getUnifiedNativeAd(), adView);
                adViewHolder.frameLayout.removeAllViews();
                adViewHolder.frameLayout.addView(adView);
            }*/

            //....Facebook Native Ad
            final NativeAdViewHolder adHolder = (NativeAdViewHolder) holder;

            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) adHolder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            final com.facebook.ads.NativeAd nativeAd = (com.facebook.ads.NativeAd) arrayListExploreDatas.get(position).getAd();

            if(nativeAd!=null) {
                nativeAd.unregisterView();

                adHolder.itemView.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;

                // Add the AdChoices icon
                AdChoicesView adChoicesView = new AdChoicesView(mActivity, nativeAd, true);
                adHolder.adChoicesContainer.removeAllViews();
                adHolder.adChoicesContainer.addView(adChoicesView, 0);
                // Set the Text.
                adHolder.nativeAdTitle.setText(nativeAd.getAdvertiserName());
                adHolder.nativeAdBody.setText(nativeAd.getAdBodyText());
                adHolder.nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                adHolder.nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                adHolder.nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
                adHolder.sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

                // Create a list of clickable views
                List<View> clickableViews = new ArrayList<>();
                clickableViews.add(adHolder.nativeAdTitle);
                clickableViews.add(adHolder.nativeAdCallToAction);

                // Register the Title and CTA button to listen for clicks.
                nativeAd.registerViewForInteraction(
                        adHolder.itemView,
                        adHolder.nativeAdMedia,
                        adHolder.nativeAdIcon,
                        clickableViews);
            }else {
                adHolder.itemView.getLayoutParams().height=0;
            }

        }else {
            final ExploreResponseDatas exploreResponseDatas = arrayListExploreDatas.get(position);

            String productName = exploreResponseDatas.getProductName();
            String productPrice = exploreResponseDatas.getPrice();
            String currency = getCurrencySymbol(exploreResponseDatas.getCurrency());
            String pricetag = currency+" "+productPrice;
            int isSwap=exploreResponseDatas.getIsSwap();

            final MyViewHolder exploreHoler= (MyViewHolder) holder;

            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) exploreHoler.itemView.getLayoutParams();
            layoutParams.setFullSpan(false);

            // set product name
            productName=CommonClass.getFirstCaps(productName);
            if (productName != null && !productName.isEmpty())
                exploreHoler.tV_productName.setText(productName);

            // set product price
            if (productPrice != null && !productPrice.isEmpty())
                exploreHoler.tV_productPrice.setText(pricetag);


            String postedImageUrl = exploreResponseDatas.getMainUrl();

            String containerHeight = exploreResponseDatas.getContainerHeight();
            String containerWidth = exploreResponseDatas.getContainerWidth();
            String isPromoted = exploreResponseDatas.getIsPromoted();

            int deviceHalfWidth = CommonClass.getDeviceWidth(mActivity) / 2;
            int setHeight = 0;

            if (containerWidth != null && !containerWidth.isEmpty())
                setHeight = (Integer.parseInt(containerHeight) * deviceHalfWidth) / (Integer.parseInt(containerWidth));

            if(setHeight>CommonClass.dpToPx(mActivity,250))
                setHeight=CommonClass.dpToPx(mActivity,250);

            exploreHoler.iV_explore_img.getLayoutParams().height = setHeight;

            System.out.println(TAG + " " + "containerHeight=" + containerHeight + " " + "set height=" + setHeight + " " + "device half height=" + " " + CommonClass.getDeviceWidth(mActivity) + " " + CommonClass.getDeviceWidth(mActivity) / 2);

            String imageUrl=postedImageUrl.replace("upload/","upload/c_fit,h_500,q_40,w_500/");

            // product image
            try {
                Glide.with(mActivity)
                        .load(imageUrl)
                        .fitCenter()
                        .placeholder(R.color.image_bg_color)
                        .error(R.color.image_bg_color)
                        .into(exploreHoler.iV_explore_img);
            } catch (OutOfMemoryError | IllegalArgumentException | NullPointerException e) {
                e.printStackTrace();
            }

            ViewCompat.setTransitionName(exploreHoler.iV_explore_img, exploreResponseDatas.getProductName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animalItemClickListener.onItemClick(holder.getAdapterPosition(), exploreHoler.iV_explore_img);
                }
            });

            // show featured tag with product
            if (isPromoted != null && !isPromoted.isEmpty()) {
                if (!isPromoted.equals("0")) {
                    exploreHoler.rL_featured.setVisibility(View.VISIBLE);
                } else exploreHoler.rL_featured.setVisibility(View.GONE);
            } else exploreHoler.rL_featured.setVisibility(View.GONE);

            if(isSwap==1) {
                exploreHoler.rL_swap_tag.setVisibility(View.VISIBLE);
                String listOfSwapItems="";
                if(exploreResponseDatas.getSwapPost()!=null && exploreResponseDatas.getSwapPost().size()>0) {
                    for (SwapPost s : exploreResponseDatas.getSwapPost())
                        listOfSwapItems +=s.getSwapTitle()+", ";

                    String swap = mActivity.getString(R.string.swap_for_colon) + " " + listOfSwapItems.substring(0, listOfSwapItems.length() - 2);
                    exploreHoler.tV_swap_item.setText(swap);
                    exploreHoler.tV_swap_item.setVisibility(View.VISIBLE);
                }
            }
            else {
                exploreHoler.rL_swap_tag.setVisibility(View.GONE);
                exploreHoler.tV_swap_item.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return arrayListExploreDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        /*if(position%10==0 && position!=0) {
            if(arrayListExploreDatas.get(position).getAd()!=null)
                return AD_VIEW;
            else
                return ITEM_VIEW;
        }
        else {
            return ITEM_VIEW;
        }*/
        return ITEM_VIEW;
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView iV_explore_img;
        private RelativeLayout rL_featured,rL_swap_tag;
        private TextView tV_productPrice,tV_productName,tV_swap_item;

        MyViewHolder(View itemView) {
            super(itemView);

            tV_productName = (TextView) itemView.findViewById(R.id.tV_productName);
            tV_productPrice = (TextView) itemView.findViewById(R.id.tV_productPrice);
            iV_explore_img= (ImageView) itemView.findViewById(R.id.iV_image);
            rL_featured= (RelativeLayout) itemView.findViewById(R.id.rL_featured);
            rL_swap_tag= (RelativeLayout) itemView.findViewById(R.id.rL_swap_tag);
            tV_swap_item = (TextView) itemView.findViewById(R.id.tV_swap_item);
            rL_featured.setVisibility(View.GONE);
        }
    }

    // admob native ad view holder
    class AdViewHolder extends RecyclerView.ViewHolder{

        private FrameLayout frameLayout;

        AdViewHolder(View itemView) {
            super(itemView);
            frameLayout = (FrameLayout)itemView.findViewById(R.id.fl_adplaceholder);
        }
    }

    //facebook native ads view holder
    private static class NativeAdViewHolder extends RecyclerView.ViewHolder {
        AdIconView nativeAdIcon;
        TextView nativeAdTitle;
        com.facebook.ads.MediaView nativeAdMedia;
        TextView nativeAdSocialContext;
        TextView nativeAdBody;
        TextView sponsoredLabel;
        Button nativeAdCallToAction;
        LinearLayout adChoicesContainer;


        NativeAdViewHolder(View itemView) {
            super(itemView);
            adChoicesContainer = (LinearLayout) itemView.findViewById(R.id.ad_choices_container);
            nativeAdIcon = (AdIconView) itemView.findViewById(R.id.native_ad_icon);
            nativeAdTitle = (TextView) itemView.findViewById(R.id.native_ad_title);
            nativeAdMedia = (com.facebook.ads.MediaView) itemView.findViewById(R.id.native_ad_media);
            nativeAdSocialContext = (TextView) itemView.findViewById(R.id.native_ad_social_context);
            nativeAdBody = (TextView) itemView.findViewById(R.id.native_ad_body);
            sponsoredLabel = (TextView) itemView.findViewById(R.id.native_ad_sponsored_label);
            nativeAdCallToAction = (Button) itemView.findViewById(R.id.native_ad_call_to_action);
        }
    }

    private String getCurrencySymbol(String currency){
        if (currency!=null && !currency.isEmpty()) {
            //..from locale..//
            /*Currency c  = Currency.getInstance(currency);
            currency=c.getSymbol();
            tV_currency.setText(currency);*/

            //..from array..//
            String[] arrayCurrency=mActivity.getResources().getStringArray(R.array.currency_picker);

            if (arrayCurrency.length>0) {
                String[] getCurrencyArr;
                for (String setCurrency : arrayCurrency) {
                    getCurrencyArr = setCurrency.split(",");
                    String currency_code = getCurrencyArr[1];
                    String currency_symbol = getCurrencyArr[2];

                    if (currency.equals(currency_code)) {
                        System.out.println(TAG + " " + "currency symbol=" + currency_symbol + " " + "my currency=" + currency);
                        return currency_symbol;
                    }
                }
            }
        }
        return currency;
    }


    /*/*//**
     * Populates a {@link UnifiedNativeAdView} object with data from a given
     * {@link UnifiedNativeAd}.
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView          the view to be populated
     *//**//**/
    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
        // VideoController will call methods on this object when events occur in the video
        // lifecycle.
        vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            public void onVideoEnd() {
                // Publishers should allow native ads to complete video playback before refreshing
                // or replacing them with another ad in the same UI location.
                //refresh.setEnabled(true);
                //videoStatus.setText("Video status: Video playback has ended.");
                super.onVideoEnd();
            }
        });

        MediaView mediaView = (MediaView) adView.findViewById(R.id.ad_media);
        ImageView mainImageView = (ImageView) adView.findViewById(R.id.ad_image);

        // Apps can check the VideoController's hasVideoContent property to determine if the
        // NativeAppInstallAd has a video asset.
        if (vc.hasVideoContent()) {
            adView.setMediaView(mediaView);
            mainImageView.setVisibility(View.GONE);
            /*videoStatus.setText(String.format(Locale.getDefault(),
                    "Video status: Ad contains a %.2f:1 video asset.",
                    vc.getAspectRatio()));*/
        } else {
            adView.setImageView(mainImageView);
            mediaView.setVisibility(View.GONE);

            // At least one image is guaranteed.
            List<NativeAd.Image> images = nativeAd.getImages();
            mainImageView.setImageDrawable(images.get(0).getDrawable());

            //refresh.setEnabled(true);
           // videoStatus.setText("Video status: Ad does not contain a video asset.");
        }

        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        adView.setNativeAd(nativeAd);
    }
}
