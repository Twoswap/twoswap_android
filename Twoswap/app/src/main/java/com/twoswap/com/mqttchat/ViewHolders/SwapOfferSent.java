package com.twoswap.com.mqttchat.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;

/**
 * Created by embed on 10/5/18.
 */

public class SwapOfferSent extends RecyclerView.ViewHolder {

    public ImageView iV_otherProduct,iV_ownProduct;
    public TextView tV_swapMessage;

    public SwapOfferSent(View itemView) {
        super(itemView);

        iV_ownProduct = (ImageView) itemView.findViewById(R.id.iV_ownProduct);
        iV_otherProduct = (ImageView) itemView.findViewById(R.id.iV_otherProduct);
        tV_swapMessage = (TextView) itemView.findViewById(R.id.tV_swapMessage);
    }
}
