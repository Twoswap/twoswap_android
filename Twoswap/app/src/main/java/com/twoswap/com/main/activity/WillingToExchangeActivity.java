package com.twoswap.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.IwantExchangeAdapter;
import com.twoswap.com.adapter.IwantSuggessionAdapter;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.pojo_class.exchange_suggtions_pojo.MyExchangesData;
import com.twoswap.com.pojo_class.exchange_suggtions_pojo.MyExchangesPojo;
import com.twoswap.com.pojo_class.product_details_pojo.SwapPost;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.SessionManager;
import com.twoswap.com.utility.VariableConstants;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class WillingToExchangeActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationMessageDialog mNotificationMessageDialog;
    private static final String TAG = PostProductActivity.class.getSimpleName();
    private Activity mActivity;
    private EditText eT_searchProduct;
    private ImageView crossIv;
    private TextView tV_save;
    private ArrayList<SwapPost> iWantArrayList;
    private IwantExchangeAdapter iwantAdapter;
    private IwantSuggessionAdapter iwantSuggessionAdapter;
    private RecyclerView iwantRv, suggessionRv;
    private ArrayList<MyExchangesData> arrayList = new ArrayList();
    private SessionManager mSessionManager;
    private LinearLayout rL_rootElement;
    private ArrayList<String> arrayListPostIds = new ArrayList<>();
    private boolean addItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_willing_to_exchange);
        intializeVariable();

    }

    private void intializeVariable() {

        mActivity = WillingToExchangeActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        Intent intent = getIntent();
        iWantArrayList = (ArrayList<SwapPost>) intent.getSerializableExtra("iWantArrayList");


        for (int i = 0; i < iWantArrayList.size(); i++) {
            SwapPost iwantItemPojo = iWantArrayList.get(i);
            if (!iwantItemPojo.getItemType())
                arrayListPostIds.add(iwantItemPojo.getSwapPostId());
           if (iwantItemPojo.getItemType()){
               iWantArrayList.remove(iwantItemPojo);
           }
        }
        mSessionManager = new SessionManager(mActivity);

        eT_searchProduct = (EditText) findViewById(R.id.eT_searchProduct);
        crossIv = (ImageView) findViewById(R.id.crossIv);
        tV_save = (TextView) findViewById(R.id.tV_save);
        iwantRv = (RecyclerView) findViewById(R.id.iwantRv);
        suggessionRv = (RecyclerView) findViewById(R.id.suggessionRv);
        rL_rootElement = (LinearLayout) findViewById(R.id.rL_rootElement);

        tV_save.setOnClickListener(this);
        crossIv.setOnClickListener(this);

        iwantAdapter = new IwantExchangeAdapter(iWantArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        iwantRv.setLayoutManager(layoutManager);
        iwantRv.setAdapter(iwantAdapter);

        iwantSuggessionAdapter = new IwantSuggessionAdapter(arrayList, this);
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        suggessionRv.setLayoutManager(mlayoutManager);
        suggessionRv.setAdapter(iwantSuggessionAdapter);


        eT_searchProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                getExchangeData(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getExchangeData(String productName) {

        if (CommonClass.isNetworkAvailable(mActivity)) {
            String url = ApiUrl.GET_EXCHAGES_BY_PRODUCT_NAME + "?token=" + mSessionManager.getAuthToken() + "&productName=" + productName;

            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET, new JSONObject(), new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    //pBar_resend.setVisibility(View.GONE);
                    System.out.println(TAG + " " + mSessionManager.getAuthToken());

                    System.out.println(TAG + " " + "post product  res=" + result);

                    MyExchangesPojo myExchangesPojo;
                    Gson gson = new Gson();
                    myExchangesPojo = gson.fromJson(result, MyExchangesPojo.class);

                    switch (myExchangesPojo.getCode()) {

                        case "200":
                            suggessionRv.setVisibility(View.VISIBLE);
                            arrayList.clear();
                            arrayList.addAll(myExchangesPojo.getData());
                            iwantSuggessionAdapter.notifyDataSetChanged();
                            break;
                        // auth token expired
                        case "401":
                            CommonClass.sessionExpired(mActivity);
                            break;

                        default:
                            arrayList.clear();
                            iwantSuggessionAdapter.notifyDataSetChanged();
                            //CommonClass.showSnackbarMessage(rL_rootElement, myExchangesPojo.getMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    //  pBar_resend.setVisibility(View.GONE);
                    //  tV_reSend.setVisibility(View.VISIBLE);
                }
            });
        } else
            CommonClass.showSnackbarMessage(rL_rootElement, getResources().getString(R.string.NoInternetAccess));


    }


    public void iWantAdapterClick(String postId, int position) {

        arrayListPostIds.remove(postId);
        iWantArrayList.remove(position);
        iwantAdapter.notifyDataSetChanged();


    }

    public void suggessionAdapterClick(String productName, String postId) {
        Log.d("exe", "postId" + postId);
        Log.d("exe", "arrayListPostIds" + arrayListPostIds.contains(postId));
        if (!arrayListPostIds.contains(postId)) {
            arrayListPostIds.add(postId);
            addItem = true;}

        if (addItem) {
            SwapPost iwantItemPojo = new SwapPost();
            iwantItemPojo.setSwapTitle(productName);
            iwantItemPojo.setSwapPostId(postId);
            iWantArrayList.add(iwantItemPojo);
            iwantAdapter.notifyDataSetChanged();
            addItem = false;
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tV_save) {
            callPostProduct();
        } else {
            onBackPressed();
        }

    }

    private void callPostProduct() {
        Intent postProductIntent = new Intent();
        postProductIntent.putExtra("iWantArrayList", iWantArrayList);
        setResult(VariableConstants.Willing_TO_EXCHANGE, postProductIntent);
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

}
