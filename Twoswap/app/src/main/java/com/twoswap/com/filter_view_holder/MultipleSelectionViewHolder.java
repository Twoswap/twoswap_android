package com.twoswap.com.filter_view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.twoswap.com.R;

/**
 * Created by embed on 22/5/18.
 */

public class MultipleSelectionViewHolder extends RecyclerView.ViewHolder {
    public RecyclerView rV_multiple;
    public TextView tV_fieldName;
    public MultipleSelectionViewHolder(View itemView) {
        super(itemView);
        tV_fieldName=(TextView)itemView.findViewById(R.id.tV_fieldName);
        rV_multiple = (RecyclerView) itemView.findViewById(R.id.rV_multiple);
    }
}
