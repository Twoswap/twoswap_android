package com.twoswap.com.pojo_class.exchange_suggtions_pojo;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class MyExchangesData {

    private String description;

    private String mainUrl;

    private String postedOn;

    private String productName;

    private String postId;

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getMainUrl ()
    {
        return mainUrl;
    }

    public void setMainUrl (String mainUrl)
    {
        this.mainUrl = mainUrl;
    }

    public String getPostedOn ()
    {
        return postedOn;
    }

    public void setPostedOn (String postedOn)
    {
        this.postedOn = postedOn;
    }

    public String getProductName ()
    {
        return productName;
    }

    public void setProductName (String productName)
    {
        this.productName = productName;
    }

    public String getPostId ()
    {
        return postId;
    }

    public void setPostId (String postId)
    {
        this.postId = postId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [description = "+description+", mainUrl = "+mainUrl+", postedOn = "+postedOn+", productName = "+productName+", postId = "+postId+"]";
    }
}
