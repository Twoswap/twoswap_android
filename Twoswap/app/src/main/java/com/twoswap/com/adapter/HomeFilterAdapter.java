package com.twoswap.com.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twoswap.com.R;
import com.twoswap.com.main.tab_fragments.HomeFrag;
import com.twoswap.com.pojo_class.product_category.ProductCategoryResDatas;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class HomeFilterAdapter extends RecyclerView.Adapter<HomeFilterAdapter.MyViewHolder> {

    private Activity mActivity;
    private ArrayList<ProductCategoryResDatas> aL_categoryDatas;
    private int selectedColor, unSelectedColor;
    private HomeFrag homeFrag;
    public HomeFilterAdapter(HomeFrag homeFrag,Activity mActivity, ArrayList<ProductCategoryResDatas> aL_categoryDatas) {
        this.mActivity = mActivity;
        this.aL_categoryDatas = aL_categoryDatas;
        this.homeFrag=homeFrag;
        selectedColor=ContextCompat.getColor(mActivity,R.color.heading_color);
        unSelectedColor=ContextCompat.getColor(mActivity,R.color.heading_color);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_cat_item_home_pg, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        String name;
        boolean isFilterCateSelected;
        final String seletedImage, unseletedImage;
        name = aL_categoryDatas.get(position).getName();

        // make first character of character is uppercase
        if (name != null && !name.isEmpty())
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        //aL_categoryDatas.get(position).setName(name);

        seletedImage = aL_categoryDatas.get(position).getActiveimage();
        unseletedImage = aL_categoryDatas.get(position).getDeactiveimage();
        isFilterCateSelected = aL_categoryDatas.get(position).isSelected();
        holder.tV_category.setTextColor(ContextCompat.getColor(mActivity, R.color.heading_color));

        // set values like category image and name
       /* if (isFilterCateSelected)
            setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, name, selectedColor);
        else
            setCategoryImage(holder.iV_category, unseletedImage, holder.tV_category, name, unSelectedColor);*/
        setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, name, selectedColor);

        // To select or unselect category
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isViewSelected = aL_categoryDatas.get(holder.getAdapterPosition()).isSelected();
                if (!isViewSelected) {
                    aL_categoryDatas.get(holder.getAdapterPosition()).setSelected(true);

                    //set all others to unselected
                    for (int i=0;i<aL_categoryDatas.size();i++){
                        if(i!=holder.getAdapterPosition())
                            aL_categoryDatas.get(i).setSelected(false);
                    }

                    //setCategoryImage(holder.iV_category, seletedImage, holder.tV_category, aL_categoryDatas.get(holder.getAdapterPosition()).getName(), selectedColor);
                    homeFrag.getFilterData();
                } else {
                    aL_categoryDatas.get(holder.getAdapterPosition()).setSelected(false);
                    //setCategoryImage(holder.iV_category, unseletedImage, holder.tV_category, aL_categoryDatas.get(holder.getAdapterPosition()).getName(), unSelectedColor);
                    homeFrag.getFilterData();

                }

                notifyDataSetChanged();
            }
        });


    }

    private void setCategoryImage(ImageView imageView, String url, TextView tV_category, String categoryName, int color) {
        // set image
       /* if (url!=null && !url.isEmpty())
            Picasso.with(mActivity)
                    .load(url)
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(imageView);*/

        if (url != null && !url.isEmpty())
            Glide.with(mActivity)
                    .load(url)
                    //.placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(imageView);

        // set category name
        if (categoryName != null) {
            // make first character of character is uppercase
            categoryName = categoryName.substring(0, 1).toUpperCase() + categoryName.substring(1).toLowerCase();
            tV_category.setText(categoryName);
            tV_category.setTextColor(color);
        }
    }


    @Override
    public int getItemCount() {
        return aL_categoryDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iV_category;
        private TextView tV_category;
        private View view;

        public MyViewHolder(View itemView) {
            super(itemView);

            iV_category = (ImageView) itemView.findViewById(R.id.iV_category);
            tV_category = (TextView) itemView.findViewById(R.id.tV_category);
            view = itemView;


        }
    }
}
