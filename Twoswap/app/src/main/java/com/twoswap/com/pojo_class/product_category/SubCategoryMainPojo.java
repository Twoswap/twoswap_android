package com.twoswap.com.pojo_class.product_category;

import java.util.ArrayList;

/**
 * Created by embed on 18/5/18.
 */

public class SubCategoryMainPojo {

    String code,message;
    ArrayList<SubCategoryData> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SubCategoryData> getData() {
        return data;
    }

    public void setData(ArrayList<SubCategoryData> data) {
        this.data = data;
    }
}
