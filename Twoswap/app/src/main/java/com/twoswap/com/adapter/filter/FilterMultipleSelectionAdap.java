package com.twoswap.com.adapter.filter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.main.activity.FilterActivity;
import com.twoswap.com.pojo_class.filter.SendFilter;
import com.twoswap.com.utility.ClickListener;

import java.util.ArrayList;

/**
 * Created by embed on 17/5/18.
 */

public class FilterMultipleSelectionAdap extends RecyclerView.Adapter<FilterMultipleSelectionAdap.MyViewHolder> {

    private Context mContext;
    private Activity mActivity;
    private ArrayList<String> data;
    public ArrayList<String> selectedData;
    private String fieldName;
    private ClickListener clickListener;

    public FilterMultipleSelectionAdap(Context mContext, ArrayList<String> data,String fieldName) {
        this.mContext=mContext;
        this.mActivity = (Activity) mContext;
        this.data=data;
        this.clickListener= clickListener;
        this.fieldName=fieldName;
        selectedData=new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.single_row_filter_multiple_selecetion,null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(selectedData.contains(data.get(position))){
            holder.iV_select.setVisibility(View.VISIBLE);
        }else {
            holder.iV_select.setVisibility(View.GONE);
        }


        holder.tV_value.setText(data.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mActivity.getWindow().getCurrentFocus()!=null)
                    mActivity.getWindow().getCurrentFocus().clearFocus();

               // if selected list contains values then remove from selection else add to selected list

               if(selectedData.contains(data.get(holder.getAdapterPosition()))){
                   selectedData.remove(data.get(holder.getAdapterPosition()));
                   removeFromSendFilterList(data.get(holder.getAdapterPosition()));
               }else {
                   selectedData.add(data.get(holder.getAdapterPosition()));
                   addToSendFilterList(data.get(holder.getAdapterPosition()));
               }

               //addSelectedValueToFilterKeyValue();
               notifyItemRangeChanged(0,getItemCount());

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tV_value;
        ImageView iV_select;
        public MyViewHolder(View itemView) {
            super(itemView);
            tV_value = (TextView) itemView.findViewById(R.id.tV_value);
            iV_select = (ImageView) itemView.findViewById(R.id.iV_select);
        }
    }

    private void addToSendFilterList(String value){
        ((FilterActivity) mContext).sendFilters.add(new SendFilter("equlTo",fieldName,value));
        //print to check
        for(SendFilter f:((FilterActivity)mContext).sendFilters){
            System.out.println("selected values="+f.getFiledName()+","+f.getValue());
        }
    }
    private void removeFromSendFilterList(String value){
        for(int i=0;i<((FilterActivity)mContext).sendFilters.size();i++) {
            if(((FilterActivity) mContext).sendFilters.get(i).getType().equals("equlTo")){
                if (((FilterActivity) mContext).sendFilters.get(i).getValue().equals(value)) {
                    ((FilterActivity) mContext).sendFilters.remove(i);
                    break;
                }
            }
        }

        //print to check
        for(SendFilter f:((FilterActivity)mContext).sendFilters){
            System.out.println("selected values="+f.getFiledName()+","+f.getValue());
        }
    }

}
