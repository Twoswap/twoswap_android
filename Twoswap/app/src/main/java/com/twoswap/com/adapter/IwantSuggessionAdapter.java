package com.twoswap.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.main.activity.WillingToExchangeActivity;
import com.twoswap.com.pojo_class.exchange_suggtions_pojo.MyExchangesData;


import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class IwantSuggessionAdapter extends RecyclerView.Adapter<IwantSuggessionAdapter.MyViewHolder> {

    private ArrayList<MyExchangesData> arrayList;
    private WillingToExchangeActivity mActivity;

    public IwantSuggessionAdapter(ArrayList<MyExchangesData> arrayList, WillingToExchangeActivity mActivity) {
        this.arrayList = arrayList;
        this.mActivity = mActivity;
    }

    @Override
    public IwantSuggessionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_iwant_suggession_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(IwantSuggessionAdapter.MyViewHolder holder, int position) {
        final MyExchangesData myExchangesData = arrayList.get(position);
        final String productName = myExchangesData.getProductName();
        final String postId = myExchangesData.getPostId();
        holder.tV_suggession.setText(productName);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.suggessionAdapterClick(productName, postId);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tV_suggession;

        public MyViewHolder(View itemView) {
            super(itemView);
            tV_suggession = (TextView) itemView.findViewById(R.id.tV_suggession);

        }
    }

}
