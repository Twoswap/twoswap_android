package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.VariableConstants;

public class EditTextTypeActivity extends AppCompatActivity implements View.OnClickListener{

    private NotificationMessageDialog mNotificationMessageDialog;
    private RelativeLayout rL_done;
    private TextView tV_fieldName;
    private EditText eT_fieldValue;
    private String fieldName;
    private TextView tV_title;
    private String title;
    private String type;
    private String selectedValue="";
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_view);

        mActivity=EditTextTypeActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);


        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");

        if(getIntent().hasExtra("isNumber")){
            type=getIntent().getStringExtra("isNumber");
        }

        if(getIntent().hasExtra("selectedValue")){
            selectedValue=getIntent().getStringExtra("selectedValue");
        }


        tV_title=(TextView)findViewById(R.id.tV_title);
        tV_title.setText(title);

        tV_fieldName=(TextView)findViewById(R.id.tV_fieldName);
        tV_fieldName.setText(fieldName);

        eT_fieldValue=(EditText)findViewById(R.id.eT_fieldValue);
        //eT_fieldValue.setHint(fieldName);

        if(selectedValue!=null && !selectedValue.isEmpty())
            eT_fieldValue.setText(selectedValue);

        if(type.equals("1")){
            eT_fieldValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        rL_done = (RelativeLayout)findViewById(R.id.rL_done);
        rL_done.setOnClickListener(this);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                Intent intent=getIntent();
                intent.putExtra("selectedValue",eT_fieldValue.getText().toString().trim());
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

}

