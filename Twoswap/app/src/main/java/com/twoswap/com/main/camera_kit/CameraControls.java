package com.twoswap.com.main.camera_kit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;
import com.twoswap.com.R;
import com.twoswap.com.utility.CapturedImage;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.CommonClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;

public class CameraControls extends LinearLayout {

    private int cameraViewId = -1;
    private CameraView cameraView;

    private int coverViewId = -1;
    private View coverView;

    @BindView(R.id.galleryButton)
    ImageView galleryButton;

    @BindView(R.id.flashButton)
    ImageView flashButton;

    @BindView(R.id.tV_timer)
    TextView tV_timer;

    private long captureDownTime;
    private long captureStartTime;
    private boolean pendingVideoCapture;
    private boolean capturingVideo;
    private Activity mActivity;
    private ClickListener clickListener;

    /**
     * This is the output file for our picture.
     */
    private File mFile=null;

    public CameraControls(Context context) {
        this(context, null);
        mActivity= (Activity) context;
        clickListener= (ClickListener) context;
    }

    public CameraControls(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        mActivity= (Activity) context;
        clickListener= (ClickListener) context;
    }

    public CameraControls(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mActivity= (Activity) context;
        clickListener= (ClickListener) context;
        LayoutInflater.from(getContext()).inflate(R.layout.camera_controls, this);
        ButterKnife.bind(this);

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CameraControls,
                    0, 0);

            try {
                cameraViewId = a.getResourceId(R.styleable.CameraControls_camera, -1);
                coverViewId = a.getResourceId(R.styleable.CameraControls_cover, -1);
            } finally {
                a.recycle();
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (cameraViewId != -1) {
            View view = getRootView().findViewById(cameraViewId);
            if (view instanceof CameraView) {
                cameraView = (CameraView) view;
                cameraView.setSoundEffectsEnabled(false);
                cameraView.bindCameraKitListener(this);
                //setFacingImageBasedOnCamera();
            }
        }

        if (coverViewId != -1) {
            View view = getRootView().findViewById(coverViewId);
            if (view != null) {
                coverView = view;
                coverView.setVisibility(GONE);
            }
        }
    }

    private void setFacingImageBasedOnCamera() {
        if (cameraView.isFacingFront()) {
            galleryButton.setImageResource(R.drawable.ic_facing_back);
        } else {
            galleryButton.setImageResource(R.drawable.ic_facing_front);
        }
    }

    //@OnCameraKitEvent(CameraKitImage.class)
    public void imageCaptured(CameraKitImage image) {
        byte[] jpeg = image.getJpeg();

        /*Bitmap b = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
        int width=CommonClass.getDeviceWidth(mActivity);
        int height=CommonClass.getDeviceHeight(mActivity);
        height=height-CommonClass.dpToPx(mActivity,140);
        b=getResizedBitmap(b,height,width);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        jpeg = stream.toByteArray();
        b.recycle();*/

        if(jpeg!=null)
            saveImage(jpeg);

        long callbackTime = System.currentTimeMillis();
        ResultHolder.dispose();
        ResultHolder.setImage(jpeg);
        ResultHolder.setNativeCaptureSize(cameraView.getCaptureSize());
        ResultHolder.setTimeToCallback(callbackTime - captureStartTime);
       /* Intent intent = new Intent(getContext(), PreviewActivity.class);
        getContext().startActivity(intent);*/
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

//    @OnCameraKitEvent(CameraKitVideo.class)
    public void videoCaptured(CameraKitVideo video) {
        File videoFile = video.getVideoFile();
        if (videoFile != null) {
            ResultHolder.dispose();
            ResultHolder.setVideo(videoFile);
            saveVideoFile(videoFile);
            ResultHolder.setNativeCaptureSize(cameraView.getCaptureSize());
            Intent intent = new Intent(getContext(), PreviewActivity.class);
            getContext().startActivity(intent);
        }
    }

    @OnTouch(R.id.captureButton)
    boolean onTouchCapture(View view, MotionEvent motionEvent) {
        handleViewTouchFeedback(view, motionEvent);
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                /*captureDownTime = System.currentTimeMillis();
                pendingVideoCapture = true;

                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pendingVideoCapture) {

                            //for timer
                            tV_timer.setVisibility(VISIBLE);
                            startTime = SystemClock.uptimeMillis();
                            customHandler.postDelayed(updateTimerThread, 0);

                            capturingVideo = true;
                            cameraView.captureVideo(new CameraKitEventCallback<CameraKitVideo>() {
                                @Override
                                public void callback(CameraKitVideo cameraKitVideo) {
                                    videoCaptured(cameraKitVideo);
                                }
                            });
                        }
                    }
                }, 250);
                break;*/
            }

            case MotionEvent.ACTION_UP: {
                pendingVideoCapture = false;

                if (capturingVideo) {
                    capturingVideo = false;
                    cameraView.stopVideo();

                    // for timer
                    timeSwapBuff += timeInMilliseconds;
                    customHandler.removeCallbacks(updateTimerThread);
                    tV_timer.setText("");
                    tV_timer.setVisibility(INVISIBLE);

                } else {
                    if(((CameraKitActivity)mActivity).arrayListImgPath.size()<5) {
                        captureStartTime = System.currentTimeMillis();
                        cameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
                            @Override
                            public void callback(CameraKitImage event) {
                                imageCaptured(event);
                            }
                        });
                    }else {
                        CommonClass.showTopSnackBar(((CameraKitActivity)mActivity).rL_rootview,mActivity.getString(R.string.you_can_select_only_upto));
                    }
                }
                break;
            }
        }
        return true;
    }

    @OnTouch(R.id.galleryButton)
    boolean onTouchFacing(final View view, MotionEvent motionEvent) {
        handleViewTouchFeedback(view, motionEvent);
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_UP: {
                clickListener.onItemClick(galleryButton,1);
                //..this is for change the camera facing.
                /*coverView.setAlpha(0);
                coverView.setVisibility(VISIBLE);
                coverView.animate()
                        .alpha(1)
                        .setStartDelay(0)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                if (cameraView.isFacingFront()) {
                                    cameraView.setFacing(CameraKit.Constants.FACING_BACK);
                                    changeViewImageResource((ImageView) view, R.drawable.ic_facing_front);
                                } else {
                                    cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
                                    changeViewImageResource((ImageView) view, R.drawable.ic_facing_back);
                                }

                                coverView.animate()
                                        .alpha(0)
                                        .setStartDelay(200)
                                        .setDuration(300)
                                        .setListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                super.onAnimationEnd(animation);
                                                coverView.setVisibility(GONE);
                                            }
                                        })
                                        .start();
                            }
                        })
                        .start();

                break;*/
            }
        }
        return true;
    }

    @OnTouch(R.id.flashButton)
    boolean onTouchFlash(View view, MotionEvent motionEvent) {
        handleViewTouchFeedback(view, motionEvent);
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_UP: {
                if (cameraView.getFlash() == CameraKit.Constants.FLASH_OFF) {
                    cameraView.setFlash(CameraKit.Constants.FLASH_ON);
                    changeViewImageResource((ImageView) view, R.drawable.flash_light_on);
                } else {
                    cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
                    changeViewImageResource((ImageView) view, R.drawable.flash_light_off);
                }

                break;
            }
        }
        return true;
    }

    boolean handleViewTouchFeedback(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                touchDownAnimation(view);
                return true;
            }

            case MotionEvent.ACTION_UP: {
                touchUpAnimation(view);
                return true;
            }

            default: {
                return true;
            }
        }
    }

    void touchDownAnimation(View view) {
        view.animate()
                .scaleX(0.88f)
                .scaleY(0.88f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    void touchUpAnimation(View view) {
        view.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    void changeViewImageResource(final ImageView imageView, @DrawableRes final int resId) {
        imageView.setRotation(0);
        imageView.animate()
                .rotationBy(360)
                .setDuration(400)
                .setInterpolator(new OvershootInterpolator())
                .start();

        imageView.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.setImageResource(resId);
            }
        }, 120);
    }

    private void saveImage(byte[] jpeg){
        String folderPath;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            folderPath = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name);
        } else {
            folderPath = getContext().getFilesDir() + "/" + getResources().getString(R.string.app_name);
        }

        File folder = new File(folderPath);
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }
        if(mFile==null) {
            mFile = new File(folderPath, String.valueOf(System.currentTimeMillis()) + ".jpg");
        }
        try {
            if (!mFile.exists()) {
                mFile.createNewFile();
            }
        } catch (Exception e) {
            Toast.makeText(this.getContext(), "Image Not saved", Toast.LENGTH_SHORT).show();
        }

        FileOutputStream output = null;
        try {
            output = new FileOutputStream(mFile);
            output.write(jpeg);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != output) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(getContext().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // set imageView to CameraKitActivity horizontal images list
                CapturedImage image = new CapturedImage();
                image.setImagePath(mFile.getAbsolutePath());
                image.setRotateAngle(0);
                ((CameraKitActivity)mActivity).arrayListImgPath.add(image);

                ((CameraKitActivity)mActivity).imagesHorizontalRvAdap.notifyItemInserted(((CameraKitActivity)mActivity).arrayListImgPath.size());
                //imagesHorizontalRvAdap.notifyDataSetChanged();

                ((CameraKitActivity)mActivity).isToCaptureImage = ((CameraKitActivity)mActivity).arrayListImgPath.size() < 5;

                // enable upload button
                if (((CameraKitActivity)mActivity).arrayListImgPath.size() > 0)
                    ((CameraKitActivity)mActivity).tV_upload.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));

                mFile=null;

            }
        };
        mainHandler.post(myRunnable);
    }

    private void saveVideoFile(File videoFile){
        String folderPath;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            folderPath = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name);
        } else {
            folderPath = getContext().getFilesDir() + "/" + getResources().getString(R.string.app_name);
        }

        File folder = new File(folderPath);
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }

        InputStream in = null;
        OutputStream out = null;
        try {
            Log.d("video file path",videoFile.getAbsolutePath()+"\n"+videoFile.getPath()+"\n"+videoFile.getCanonicalPath());
            in = new FileInputStream(videoFile.getAbsolutePath());
            out = new FileOutputStream(folderPath + "/" + String.valueOf(System.currentTimeMillis())+".mp4");

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    private long startTime = 0L;

    private Handler customHandler = new Handler();

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            tV_timer.setText("" + mins + ":"
                    + String.format("%02d", secs));
            customHandler.postDelayed(this, 0);
        }

    };

}
