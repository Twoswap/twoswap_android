package com.twoswap.com.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesData;
import com.twoswap.com.utility.CircleTransform;
import com.twoswap.com.utility.CommonClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class MyExchangesAdapter extends RecyclerView.Adapter<MyExchangesAdapter.MyViewHolder> {

    private static final String TAG = ExploreRvAdapter.class.getSimpleName();
    private Activity mActivity;
    private ArrayList<ProfileMyExchangesData> profileMyExchangesDataArrayList;

    public MyExchangesAdapter(ArrayList<ProfileMyExchangesData> profileMyExchangesDataArrayList, Activity mActivity) {
        this.profileMyExchangesDataArrayList = profileMyExchangesDataArrayList;
        this.mActivity = mActivity;
    }

    @Override
    public MyExchangesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_profile_myexchanges, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyExchangesAdapter.MyViewHolder holder, int position) {
        final ProfileMyExchangesData profileMyExchangesData = profileMyExchangesDataArrayList.get(position);

        String mainUrl = profileMyExchangesData.getMainUrl();
        String thumbNailUrl = profileMyExchangesData.getThumbnailImageUrl();
        String swapUrl = profileMyExchangesData.getSwapThumbnailImageUrl();
        String time = profileMyExchangesData.getAcceptedOn();
        holder.myPostIv.getLayoutParams().width = CommonClass.getDeviceWidth(mActivity) / 9;
        holder.myPostIv.getLayoutParams().height = CommonClass.getDeviceWidth(mActivity) / 9;
        holder.swapIv.getLayoutParams().width = CommonClass.getDeviceWidth(mActivity) / 9;
        holder.swapIv.getLayoutParams().height = CommonClass.getDeviceWidth(mActivity) / 9;
        System.out.println(TAG + " " + "thumbNailUrl" + thumbNailUrl);

        // main image
        try {
            if (mainUrl != null && !mainUrl.isEmpty())

                Picasso.with(mActivity)
                        .load(mainUrl)
                        .resize(CommonClass.getDeviceWidth(mActivity),CommonClass.getDeviceWidth(mActivity))
                        .centerCrop()
                        .placeholder(R.color.add_title)
                        .error(R.color.add_title)
                        .into(holder.iV_image);
                /*Glide.with(mActivity)
                        .load(mainUrl)
                        .asBitmap()
                        .centerCrop()
                        .placeholder(R.color.add_title)
                        .error(R.color.add_title)
                        .into(holder.iV_image)*/;
        } catch (OutOfMemoryError | IllegalArgumentException e) {
            e.printStackTrace();
        }

        //Thumb image
        if (thumbNailUrl != null && !thumbNailUrl.isEmpty()) {
            Picasso.with(mActivity)
                    .load(thumbNailUrl)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(holder.myPostIv);
        }

        //swap image
        if (swapUrl != null && !swapUrl.isEmpty()) {
            Picasso.with(mActivity)
                    .load(swapUrl)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_circle_img)
                    .error(R.drawable.default_circle_img)
                    .into(holder.swapIv);
        }

        // Posted on
        if (time != null && !time.isEmpty())
            holder.acceptedOnTv.setText(CommonClass.getTimeDifference(time));

        holder.descriptionTv.setText("You swaped "+profileMyExchangesData.getProductName()+" "+"for "+profileMyExchangesData.getSwapProductName());

    }

    @Override
    public int getItemCount() {

        return profileMyExchangesDataArrayList.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iV_image, myPostIv, swapIv;
        private TextView acceptedOnTv,descriptionTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            iV_image = (ImageView) itemView.findViewById(R.id.iV_image);
            myPostIv = (ImageView) itemView.findViewById(R.id.myPostIv);
            swapIv = (ImageView) itemView.findViewById(R.id.swapIv);
            acceptedOnTv = (TextView) itemView.findViewById(R.id.acceptedOnTv);
            descriptionTv=(TextView)itemView.findViewById(R.id.descriptionTv);
        }

    }


}
