package com.twoswap.com.pojo_class.profile_myexchanges_pojo;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class ProfileMyExchangesMainPojo {

    private String message;

    private ArrayList<ProfileMyExchangesData> data;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<ProfileMyExchangesData> getData ()
    {
        return data;
    }

    public void setData (ArrayList<ProfileMyExchangesData> data)
    {
        this.data = data;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+", code = "+code+"]";
    }

}
