package com.twoswap.com.pojo_class.product_category;

import java.io.Serializable;

/**
 * Created by embed on 18/5/18.
 */

public class FilterKeyValue implements Serializable {

    //type use for identify number or text data in edittext.

    String type,key,value;

    public FilterKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public FilterKeyValue(String type, String key, String value) {
        this.type = type;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
