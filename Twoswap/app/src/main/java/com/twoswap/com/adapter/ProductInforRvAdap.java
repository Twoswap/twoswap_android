package com.twoswap.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.pojo_class.product_details_pojo.PostFilter;

import java.util.ArrayList;

/**
 * Created by embed on 12/6/18.
 */

public class ProductInforRvAdap extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ArrayList<PostFilter> data;

    public ProductInforRvAdap(Context mContext, ArrayList<PostFilter> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.single_row_product_information,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MyViewHolder myViewHolder=(MyViewHolder)holder;

        myViewHolder.tV_fieldName.setText(data.get(position).getFieldName());
        myViewHolder.tV_fieldValue.setText(data.get(position).getValues());

        if(position!=0 && position%2==1){
            myViewHolder.tV_fieldName.setGravity(Gravity.RIGHT );
            myViewHolder.tV_fieldValue.setGravity(Gravity.RIGHT );
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tV_fieldName,tV_fieldValue;
        public MyViewHolder(View itemView) {
            super(itemView);

            tV_fieldName = (TextView) itemView.findViewById(R.id.tV_fieldName);
            tV_fieldValue = (TextView) itemView.findViewById(R.id.tV_fieldValue);
        }
    }
}
