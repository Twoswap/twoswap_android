package com.twoswap.com.utility;

import java.io.Serializable;

/**
 * Created by hello on 14-Sep-17.
 */

public class CapturedImage implements Serializable{
    private String imagePath;
    private int rotateAngle;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getRotateAngle() {
        return rotateAngle;
    }

    public void setRotateAngle(int rotateAngle) {
        this.rotateAngle = rotateAngle;
    }
}
