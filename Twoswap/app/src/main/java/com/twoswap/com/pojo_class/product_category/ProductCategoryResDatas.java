package com.twoswap.com.pojo_class.product_category;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hello on 2017-05-04.
 */
public class ProductCategoryResDatas implements Serializable
{
    private String categoryNodeId="",name="",deactiveimage="",activeimage="";
    private boolean isSelected;
    private int subCategoryCount;
    private int filterCount;
    private ArrayList<FilterData> filter;

    public String getCategoryNodeId() {
        return categoryNodeId;
    }

    public void setCategoryNodeId(String categoryNodeId) {
        this.categoryNodeId = categoryNodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeactiveimage() {
        return deactiveimage;
    }

    public void setDeactiveimage(String deactiveimage) {
        this.deactiveimage = deactiveimage;
    }

    public String getActiveimage() {
        return activeimage;
    }

    public void setActiveimage(String activeimage) {
        this.activeimage = activeimage;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getSubCategoryCount() {
        return subCategoryCount;
    }

    public void setSubCategoryCount(int subCategoryCount) {
        this.subCategoryCount = subCategoryCount;
    }

    public int getFilterCount() {
        return filterCount;
    }

    public void setFilterCount(int filterCount) {
        this.filterCount = filterCount;
    }

    public ArrayList<FilterData> getFilter() {
        return filter;
    }

    public void setFilter(ArrayList<FilterData> filter) {
        this.filter = filter;
    }
}
