package com.twoswap.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.ProductSubCategoryRvAdapter;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.main.view_type_activity.AddDetailActivity;
import com.twoswap.com.pojo_class.product_category.FilterKeyValue;
import com.twoswap.com.pojo_class.product_category.SubCategoryData;
import com.twoswap.com.pojo_class.product_category.SubCategoryMainPojo;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.VariableConstants;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class ProductSubCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ProductSubCategoryActivity.class.getSimpleName();
    private ProgressBar progress_bar;
    private Activity mActivity;
    private RelativeLayout rL_rootview;
    private RecyclerView rV_subCategory;
    private NotificationMessageDialog mNotificationMessageDialog;
    private ArrayList<SubCategoryData> aL_subCategoryDatas;
    private TextView tV_title;
    private String categoryName="";
    private ProductSubCategoryRvAdapter subCategoryRvAdapter;
    private String subCategoryName="";

    private String selectedCatgory="",selectedSubCategory="";
    private ArrayList<FilterKeyValue> selectedFileds;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_sub_category);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        mActivity=ProductSubCategoryActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        CommonClass.statusBarColor(mActivity);

        // get data which is selected by user.
        if(getIntent()!=null) {
            selectedSubCategory = getIntent().getStringExtra("selectedSubCategory");
            selectedFileds = (ArrayList<FilterKeyValue>) getIntent().getSerializableExtra("selectedField");
        }

        tV_title=(TextView)findViewById(R.id.tV_title);
        progress_bar= (ProgressBar) findViewById(R.id.progress_bar);
        rL_rootview= (RelativeLayout) findViewById(R.id.rL_rootview);
        rV_subCategory= (RecyclerView) findViewById(R.id.rV_category);
        RelativeLayout rL_back_btn = (RelativeLayout) findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);

        //aL_subCategoryDatas= (ArrayList<ProductCategoryResDatas>) getIntent().getSerializableExtra("categoryDatas");
        categoryName = getIntent().getStringExtra("categoryName");
        String title= categoryName.substring(0,1).toUpperCase()+""+categoryName.substring(1);
        tV_title.setText(categoryName);

        LinearLayoutManager layoutManager=new LinearLayoutManager(mActivity);
        rV_subCategory.setLayoutManager(layoutManager);

        getCategoriesService();
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * <h>GetCategoriesService</h>
     * <p>
     *     This method is called from onCreate() method of the current class.
     *     In this method we used to call the getCategories api using okHttp3.
     *     Once we get the data we show that list in recyclerview.
     * </p>
     */
    private void getCategoriesService()
    {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            progress_bar.setVisibility(View.VISIBLE);
            JSONObject request_datas = new JSONObject();

           /* try {
                request_datas.put("token", "20");
                request_datas.put("offset", "0");

            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            String Url=ApiUrl.GET_SUB_CATEGORIES+"?categoryName="+categoryName;

            OkHttp3Connection.doOkHttp3Connection(TAG, Url, OkHttp3Connection.Request_type.GET, request_datas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    progress_bar.setVisibility(View.GONE);

                    System.out.println(TAG+" "+"get subcategory res="+result);

                    SubCategoryMainPojo subCategoryMainPojo;
                    Gson gson=new Gson();
                    subCategoryMainPojo=gson.fromJson(result,SubCategoryMainPojo.class);

                    switch (subCategoryMainPojo.getCode())
                    {
                        // success
                        case "200" :
                            aL_subCategoryDatas=subCategoryMainPojo.getData();
                            subCategoryRvAdapter=new ProductSubCategoryRvAdapter(mActivity,aL_subCategoryDatas);
                            rV_subCategory.setAdapter(subCategoryRvAdapter);
                            if (aL_subCategoryDatas!=null && aL_subCategoryDatas.size()>0)
                            {
                                // set previously selected category by user
                                for(int i=0;i<aL_subCategoryDatas.size();i++){
                                    if(selectedSubCategory!=null && !selectedSubCategory.isEmpty() && selectedSubCategory.equalsIgnoreCase(aL_subCategoryDatas.get(i).getSubCategoryName()))
                                        subCategoryRvAdapter.mSelected=i;
                                }
                                /////////////////////////////////////////////

                                subCategoryRvAdapter.setOnItemClick(new ClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        subCategoryName=aL_subCategoryDatas.get(position).getSubCategoryName();

                                        // when user select another subcatgory then remove previously selected data
                                        if(!subCategoryName.equalsIgnoreCase(selectedSubCategory) && selectedFileds!=null)
                                            selectedFileds.clear();

                                        if (aL_subCategoryDatas.get(position).getFieldCount()>0) {
                                            Intent intent=new Intent(mActivity, AddDetailActivity.class);
                                            // isForFilter used while view type is open from filter screen
                                            intent.putExtra("isForFilter",getIntent().getBooleanExtra("isForFilter",false));
                                            intent.putExtra("title",categoryName+" -> "+subCategoryName);
                                            intent.putExtra("filterData",aL_subCategoryDatas.get(position).getFilter());
                                            intent.putExtra("selectedField",selectedFileds);
                                            startActivityForResult(intent,VariableConstants.ADD_DETAIL_DATA_REQ_CODE);
                                        }else {
                                            Intent data=new Intent();
                                            data.putExtra("categoryName",categoryName);
                                            data.putExtra("subCategoryName",subCategoryName);
                                            data.putExtra("filterKeyValues",new ArrayList<FilterKeyValue>());
                                            setResult(VariableConstants.SUB_CATEGORY_REQ_CODE,data);
                                            onBackPressed();
                                        }
                                    }
                                });

                            }
                            break;

                        // Error
                        default:
                            CommonClass.showSnackbarMessage(rL_rootview,subCategoryMainPojo.getMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    progress_bar.setVisibility(View.GONE);
                    CommonClass.showSnackbarMessage(rL_rootview,error);
                }
            });
        }
        else CommonClass.showSnackbarMessage(rL_rootview,getResources().getString(R.string.NoInternetAccess));
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rL_back_btn :
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null){
            switch (resultCode){
                case VariableConstants.ADD_DETAIL_DATA_REQ_CODE:
                    data.putExtra("categoryName",categoryName);
                    data.putExtra("subCategoryName",subCategoryName);
                    setResult(VariableConstants.SUB_CATEGORY_REQ_CODE,data);
                    onBackPressed();
                    break;
            }
        }
    }
}
