package com.twoswap.com.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.pojo_class.product_details_pojo.SwapPost;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class ProductDetailsSwapAdapter extends RecyclerView.Adapter<ProductDetailsSwapAdapter.MyViewHolder> {
    private ArrayList<SwapPost> arrayList;
    private Activity mActivity;

    public ProductDetailsSwapAdapter(ArrayList<SwapPost> arrayList, Activity mActivity) {
        this.arrayList = arrayList;
        this.mActivity = mActivity;
    }

    @Override
    public ProductDetailsSwapAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.single_row_iwant_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductDetailsSwapAdapter.MyViewHolder holder, int position) {

        holder.exchangeItemNameTv.setText(arrayList.get(position).getSwapTitle());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView exchangeItemNameTv;
        private ImageView crossIv;
        public MyViewHolder(View itemView) {
            super(itemView);
            exchangeItemNameTv = (TextView) itemView.findViewById(R.id.exchangeItemNameTv);
            crossIv=(ImageView)itemView.findViewById(R.id.crossIv);
            crossIv.setVisibility(View.GONE);
        }
    }
}
