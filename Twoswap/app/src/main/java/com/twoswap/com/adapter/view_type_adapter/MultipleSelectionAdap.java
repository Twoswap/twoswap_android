package com.twoswap.com.adapter.view_type_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.utility.ClickListener;

import java.util.ArrayList;

/**
 * Created by embed on 17/5/18.
 */

public class MultipleSelectionAdap extends RecyclerView.Adapter<MultipleSelectionAdap.MyViewHolder> {

    private Context mContext;
    private ArrayList<String> data;
    public ArrayList<String> selectedData;
    private ClickListener clickListener;

    public MultipleSelectionAdap(Context mContext, ArrayList<String> data) {
        this.mContext=mContext;
        this.data=data;
        this.clickListener= (ClickListener) mContext;
        selectedData=new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.single_row_selecetion_item,null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(selectedData.contains(data.get(position))){
            holder.iV_select.setVisibility(View.VISIBLE);
        }else {
            holder.iV_select.setVisibility(View.GONE);
        }


        holder.tV_value.setText(data.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // if selected list contains values then remove from selection else add to selected list

               if(selectedData.contains(data.get(holder.getAdapterPosition()))){
                   selectedData.remove(data.get(holder.getAdapterPosition()));
               }else {
                   selectedData.add(data.get(holder.getAdapterPosition()));
               }

                clickListener.onItemClick(holder.tV_value,holder.getAdapterPosition());
                notifyItemRangeChanged(0,getItemCount());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tV_value;
        ImageView iV_select;
        public MyViewHolder(View itemView) {
            super(itemView);
            tV_value = (TextView) itemView.findViewById(R.id.tV_value);
            iV_select = (ImageView) itemView.findViewById(R.id.iV_select);
        }
    }
}
