package com.twoswap.com.pojo_class.product_category;

import java.util.ArrayList;

/**
 * Created by embed on 18/5/18.
 */

public class SubCategoryData {

    private String subCategoryName;
    private int fieldCount;
    private String imageUrl;
    private ArrayList<FilterData> filter;
    private boolean isSelectd;

    public ArrayList<FilterData> getFilter() {
        return filter;
    }

    public void setFilter(ArrayList<FilterData> filter) {
        this.filter = filter;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public int getFieldCount() {
        return fieldCount;
    }

    public void setFieldCount(int fieldCount) {
        this.fieldCount = fieldCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isSelectd() {
        return isSelectd;
    }

    public void setSelectd(boolean selectd) {
        isSelectd = selectd;
    }
}
