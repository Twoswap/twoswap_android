package com.twoswap.com.main.view_pager.my_profile_frag;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.MyExchangesAdapter;
import com.twoswap.com.event_bus.BusProvider;
import com.twoswap.com.main.activity.HomePageActivity;
import com.twoswap.com.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesData;
import com.twoswap.com.pojo_class.profile_myexchanges_pojo.ProfileMyExchangesMainPojo;
import com.twoswap.com.utility.ApiUrl;
import com.twoswap.com.utility.CommonClass;
import com.twoswap.com.utility.OkHttp3Connection;
import com.twoswap.com.utility.SessionManager;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ${3embed} on ${27-10-2017}.
 * Banglore
 */

public class MyExchangesFrag extends Fragment {
    private static final String TAG = MyExchangesFrag.class.getSimpleName();
    private Activity mActivity;
    private RecyclerView rV_myprofile_exchange;
    private LinearLayoutManager layoutManager;
    private MyExchangesAdapter myExchangesAdapter;
    private ProgressBar progress_bar_profile;
    private SessionManager mSessionManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int pageIndex = 0;
    private ArrayList<ProfileMyExchangesData> profileMyExchangesData=new ArrayList<>();
    private int visibleItemCount,totalItemCount,visibleThreshold=5;
    private boolean isLoadingRequired,isToCallFirstTime;
    private RelativeLayout rL_noProductFound,rL_start_selling;
    private TextView tV_snapNpost,tV_no_ads;
    private ImageView iV_default_icon;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mSessionManager = new SessionManager(mActivity);

    }

    public static MyExchangesFrag newInstance(String memberName) {
        Bundle bundle = new Bundle();
        bundle.putString("memberName", memberName);
        MyExchangesFrag myExchangesFrag = new MyExchangesFrag();
        myExchangesFrag.setArguments(bundle);
        return myExchangesFrag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_profile_buying, container, false);
        rV_myprofile_exchange = (RecyclerView) view.findViewById(R.id.rV_myprofile_selling);
        progress_bar_profile = (ProgressBar) view.findViewById(R.id.progress_bar_profile);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        rL_noProductFound=(RelativeLayout)view.findViewById(R.id.rL_noProductFound);
        rL_start_selling=(RelativeLayout)view.findViewById(R.id.rL_start_selling);
        rL_start_selling.setVisibility(View.GONE);
        tV_snapNpost=(TextView)view.findViewById(R.id.tV_snapNpost);
        tV_no_ads=(TextView)view.findViewById(R.id.tV_no_ads);
        iV_default_icon=(ImageView)view.findViewById(R.id.iV_default_icon);
        tV_snapNpost.setVisibility(View.GONE);
        String txtMsg=getResources().getString(R.string.no_exchanges_yet)+"\n"+getResources().getString(R.string.you_have_no_exchange_yet);
        tV_no_ads.setText(txtMsg);
        iV_default_icon.setImageResource(R.drawable.empty_selling_icon);
        myExchangesAdapter = new MyExchangesAdapter(profileMyExchangesData,mActivity);
        layoutManager = new LinearLayoutManager(mActivity);
        rV_myprofile_exchange.setLayoutManager(layoutManager);
        rV_myprofile_exchange.setHasFixedSize(true);
        rV_myprofile_exchange.setAdapter(myExchangesAdapter);

        if (CommonClass.isNetworkAvailable(mActivity)) {
            progress_bar_profile.setVisibility(View.VISIBLE);
            if (mSessionManager.getIsUserLoggedIn()) {
                myExchangesPosts(pageIndex);
            }
        } else{
            CommonClass.showSnackbarMessage(((HomePageActivity) mActivity).rL_rootElement, mActivity.getResources().getString(R.string.NoInternetAccess));
        }
        mSwipeRefreshLayout.setColorSchemeResources(R.color.pink_color);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageIndex=0;
                profileMyExchangesData.clear();
                myExchangesAdapter.notifyDataSetChanged();
                if(mSessionManager.getIsUserLoggedIn()){
                    myExchangesPosts(pageIndex);
                }
            }
        });
        return view;

    }

    private void myExchangesPosts(int offset) {
        int limit = 20;
        offset = limit * offset;
        if (CommonClass.isNetworkAvailable(mActivity)) {

            System.out.println(TAG + " " + "token=" + mSessionManager.getAuthToken());

            String url=ApiUrl.MY_EXCHANGES + "?token=" + mSessionManager.getAuthToken() + "&offset=" + offset+"&limit=" +limit;

            OkHttp3Connection.doOkHttp3Connection(TAG, url, OkHttp3Connection.Request_type.GET,  new JSONObject(), new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    progress_bar_profile.setVisibility(View.GONE);
                    System.out.println(TAG + " " + "profile selling res=" + result);
                    mSwipeRefreshLayout.setRefreshing(false);

                    ProfileMyExchangesMainPojo profileMyExchangesMainPojo;
                    Gson gson = new Gson();
                    profileMyExchangesMainPojo = gson.fromJson(result,ProfileMyExchangesMainPojo.class);

                    switch (profileMyExchangesMainPojo.getCode()) {
                        // success
                        case "200":
                            if (profileMyExchangesMainPojo.getData() != null && profileMyExchangesMainPojo.getData().size() > 0) {

                                rL_noProductFound.setVisibility(View.GONE);
                                profileMyExchangesData.addAll(profileMyExchangesMainPojo.getData());
                             //   isLoadingRequired = arrayListSellingDatas.size() > 14;
                                myExchangesAdapter.notifyDataSetChanged();

                                // Load more
                                rV_myprofile_exchange.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                    @Override
                                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                        super.onScrolled(recyclerView, dx, dy);

                                        //int[] firstVisibleItemPositions = new int[2];
                                        totalItemCount = layoutManager.getItemCount();
                                        visibleItemCount = layoutManager.findLastCompletelyVisibleItemPosition();

                                        if (isLoadingRequired && totalItemCount <= (visibleItemCount + visibleThreshold)) {
                                            isLoadingRequired = false;
                                            pageIndex = pageIndex + 1;
                                            mSwipeRefreshLayout.setRefreshing(true);
                                            myExchangesPosts(pageIndex);
                                        }
                                    }
                                });
                            }
                            break;

                        // no more data found
                        case "204":
                            mSwipeRefreshLayout.setRefreshing(false);
                            if (profileMyExchangesData.size() == 0) {
                                rL_noProductFound.setVisibility(View.VISIBLE);
                            }
                            break;

                        // auth token expired
                        case "401":
                            mSwipeRefreshLayout.setRefreshing(false);
                            CommonClass.sessionExpired(mActivity);
                            break;

                        // Any error
                        default:
                            mSwipeRefreshLayout.setRefreshing(false);
                            CommonClass.showTopSnackBar(((HomePageActivity) mActivity).rL_rootElement, profileMyExchangesMainPojo.getMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    progress_bar_profile.setVisibility(View.GONE);
                    CommonClass.showSnackbarMessage(((HomePageActivity) mActivity).rL_rootElement, error);
                }
            });
        } else
            CommonClass.showSnackbarMessage(((HomePageActivity) mActivity).rL_rootElement, getResources().getString(R.string.NoInternetAccess));


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BusProvider.getInstance().register(this);

    }

    @Override
    public void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

}
