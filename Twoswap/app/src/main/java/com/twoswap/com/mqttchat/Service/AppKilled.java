package com.twoswap.com.mqttchat.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.twoswap.com.R;
import com.twoswap.com.main.activity.SplashActivity;
import com.twoswap.com.mqttchat.AppController;
import com.twoswap.com.utility.VariableConstants;

/**
 *
 * @since  21/06/17.
 * @version 1.0.
 * @author 3Embed.
 */
public class AppKilled extends Service
{
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(intent.getAction() != null && intent.getAction().equals(VariableConstants.ACTION_START_FOURGROUND) || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ){
            attachNotification();
        }
        System.out.println("AppKilled Service Start");
        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy() {
        System.out.println("AppKilled Service Destroy");
        super.onDestroy();
    }
    public void onTaskRemoved(Intent rootIntent)
    {
        AppController.getInstance().disconnect();
        AppController.getInstance().setApplicationKilled(true);
        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId());
        stopSelf();
    }

    private void attachNotification() {
        createNotificationChannel();
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),112,intent,0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.foreground_service_title))
                .setTicker("Yelo Chat")
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setChannelId(VariableConstants.BACKGROUND_SERVICE_NOTIFICATION)
                .build();
        int systemNotificationId = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(9));
        startForeground(systemNotificationId,
                notification);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getResources().getString(R.string.background_service_notification_channel);
            String description = getResources().getString(R.string.background_service_notification_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(VariableConstants.BACKGROUND_SERVICE_NOTIFICATION, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }
}