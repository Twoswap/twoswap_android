package com.twoswap.com.pojo_class.product_details_pojo;

import java.io.Serializable;

/**
 * Created by embed on 12/6/18.
 */

public class PostFilter implements Serializable {
    private String fieldName="",values="";

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }
}
