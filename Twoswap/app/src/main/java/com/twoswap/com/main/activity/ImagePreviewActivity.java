package com.twoswap.com.main.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.twoswap.com.R;
import com.twoswap.com.utility.TouchImageView;


public class ImagePreviewActivity extends AppCompatActivity {

   TouchImageView imageView;
   RelativeLayout rL_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        String url=getIntent().getStringExtra("url");

        imageView=(TouchImageView)findViewById(R.id.iV_preview);

        rL_close=(RelativeLayout)findViewById(R.id.rL_close);

        rL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(imageView);
    }
}
