package com.twoswap.com.recyleview_drag_drop;

import android.support.v7.widget.RecyclerView;

/**
 * Created by embed on 25/6/18.
 */

public interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
