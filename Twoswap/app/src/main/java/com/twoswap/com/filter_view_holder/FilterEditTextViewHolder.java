package com.twoswap.com.filter_view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.twoswap.com.R;

/**
 * Created by embed on 22/5/18.
 */

public class FilterEditTextViewHolder extends RecyclerView.ViewHolder {
    public TextView tV_fieldName;
    public EditText eT_fieldValue;
    public FilterEditTextViewHolder(View itemView) {
        super(itemView);
        tV_fieldName = (TextView)itemView.findViewById(R.id.tV_fieldName);
        eT_fieldValue = (EditText)itemView.findViewById(R.id.eT_fieldValue);
    }
}
