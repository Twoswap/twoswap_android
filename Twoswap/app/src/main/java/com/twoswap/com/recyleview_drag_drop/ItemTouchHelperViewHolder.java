package com.twoswap.com.recyleview_drag_drop;

/**
 * Created by embed on 25/6/18.
 */

public interface ItemTouchHelperViewHolder {
    /**
     * Implementations should update the item view to indicate it's active state.
     */
    void onItemSelected();


    /**
     * state should be cleared.
     */
    void onItemClear();
}
