package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.VariableConstants;

import java.util.ArrayList;
import java.util.Arrays;

public class SeekBarViewActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationMessageDialog mNotificationMessageDialog;
    private TextView tV_value,tV_title,tV_min,tV_max;
    private Activity mActivity;
    private static final String TAG=SeekBarViewActivity.class.getSimpleName();
    private String title;
    private String selectedValue="";
    private String fieldName="";
    private String values;
    private ArrayList<String> arrayListValue;
    private SeekBar seekBar;
    private RelativeLayout rootElement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seek_bar_view);

        mActivity=SeekBarViewActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);
        rootElement=(RelativeLayout)findViewById(R.id.rootElement);

        //get data from intent
        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");
        values=getIntent().getStringExtra("values");
        if(getIntent().hasExtra("selectedValue")){
            selectedValue=getIntent().getStringExtra("selectedValue");
        }

        // set activity title
        tV_title = (TextView) findViewById(R.id.tV_title);
        if(title!=null && !title.isEmpty())
            tV_title.setText(title);

        // get min and max values
        arrayListValue=getArrayListFromValues(values);
        String min=arrayListValue.get(0);
        String max=arrayListValue.get(arrayListValue.size()-1);

        // set min and max values
        tV_min=(TextView)findViewById(R.id.tV_min);
        tV_max=(TextView)findViewById(R.id.tV_max);
        if(min!=null && !min.isEmpty())
            tV_min.setText(min);
        if(max!=null && !max.isEmpty())
            tV_max.setText(max);

        seekBar =(SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(Integer.parseInt(max));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tV_value.setText(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // intialize back and done button
        RelativeLayout rL_apply=(RelativeLayout)findViewById(R.id.rL_apply);
        rL_apply.setOnClickListener(this);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rL_apply:
                Snackbar.make(rootElement,tV_value.getText().toString(), Snackbar.LENGTH_SHORT).show();
                Intent intent=getIntent();
                intent.putExtra("selectedValue",tV_value.getText().toString().trim());
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
                onBackPressed();
                break;
            case R.id.rL_back_btn:
                onBackPressed();
                break;
        }
    }

    public ArrayList<String> getArrayListFromValues(String values){
        ArrayList<String> arrayList=new ArrayList<>();

        String[] val=values.split(",");

        arrayList.addAll(Arrays.asList(val));

        return arrayList;
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
