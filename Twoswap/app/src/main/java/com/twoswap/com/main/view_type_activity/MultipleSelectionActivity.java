package com.twoswap.com.main.view_type_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twoswap.com.R;
import com.twoswap.com.adapter.view_type_adapter.MultipleSelectionAdap;
import com.twoswap.com.fcm_push_notification.Config;
import com.twoswap.com.fcm_push_notification.NotificationMessageDialog;
import com.twoswap.com.fcm_push_notification.NotificationUtils;
import com.twoswap.com.utility.ClickListener;
import com.twoswap.com.utility.VariableConstants;

import java.util.ArrayList;
import java.util.Arrays;

public class MultipleSelectionActivity extends AppCompatActivity implements ClickListener,View.OnClickListener {

    private NotificationMessageDialog mNotificationMessageDialog;
    private RecyclerView rV_singleSelection;
    private ArrayList<String> arrayList;
    private MultipleSelectionAdap multipleSelectionAdap;
    private RelativeLayout rL_done;
    private String values,fieldName;
    private TextView tV_title;
    private static  final  String TAG=MultipleSelectionActivity.class.getSimpleName();
    private String title;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_selection);

        mActivity=MultipleSelectionActivity.this;
        mNotificationMessageDialog=new NotificationMessageDialog(mActivity);

        arrayList =new ArrayList<>();
        values=getIntent().getStringExtra("values");
        title=getIntent().getStringExtra("title");
        fieldName=getIntent().getStringExtra("fieldName");
        arrayList=getArrayListFromValues(values);

        tV_title=(TextView)findViewById(R.id.tV_title);
        tV_title.setText(title);

        rV_singleSelection=(RecyclerView)findViewById(R.id.rV_singleSelection);
        multipleSelectionAdap = new MultipleSelectionAdap(this,arrayList);
        rV_singleSelection.setLayoutManager(new LinearLayoutManager(this));
        rV_singleSelection.setAdapter(multipleSelectionAdap);
        RelativeLayout rL_back_btn=(RelativeLayout)findViewById(R.id.rL_back_btn);
        rL_back_btn.setOnClickListener(this);
        rL_done=(RelativeLayout)findViewById(R.id.rL_done);
        rL_done.setOnClickListener(this);
    }

    public ArrayList<String> getArrayListFromValues(String values){
        ArrayList<String> arrayList=new ArrayList<>();

        String[] val=values.split(",");

        arrayList.addAll(Arrays.asList(val));

        return arrayList;
    }

    @Override
    public void onItemClick(View view, int position) {
        if(multipleSelectionAdap.selectedData.size()>0){
            rL_done.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rL_back_btn:
                onBackPressed();
                break;
            case R.id.rL_done:
                Intent intent=getIntent();
                String seletedData="";
                for (int i=0;i<multipleSelectionAdap.selectedData.size();i++){
                   if(i==multipleSelectionAdap.selectedData.size()-1){
                       seletedData=seletedData+multipleSelectionAdap.selectedData.get(i);
                   }else {
                       seletedData=seletedData+multipleSelectionAdap.selectedData.get(i)+",";
                   }
                }
                System.out.println(TAG+":"+"Multiple Selected Values:"+seletedData);
                intent.putExtra("selectedValue",seletedData);
                setResult(VariableConstants.SELECT_VALUE_REQ_CODE,intent);
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationMessageDialog.mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
